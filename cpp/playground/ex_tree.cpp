// Example program
#include <iostream>
#include <string>
#include <vector>
#include <optional>

template <typename ObjectType>
struct Tree
{
    std::string name;
    std::vector<Tree> childs;
    std::optional<ObjectType> object;
    
	Tree(std::string const& name_) : name(name_), childs{}, object(std::nullopt){}
    
    Tree(std::string const& name_, ObjectType const& object_) : name(name_), childs{}, object(object_) {}
};

template <typename ObjectType>
void print(Tree<ObjectType> const& tree)
{
    std::cout << "node:" << tree.name << std::endl;
	
	if (tree.object.has_value())
		std::cout << "object: " << tree.object.value() << std::endl;

    std::cout << "childs:" << std::endl;
	std::cout << "{" << std::endl;
    for (auto const& child : tree.childs)
    {
        print(child);
    }
	std::cout << "}" << std::endl;
	std::cout << std::endl;
}

int main()
{
    Tree<std::string> root("root");
    
    Tree<std::string> child1("child1", "child1 content");
    Tree<std::string> child2("child2", "child2 content");
    
    Tree<std::string> child21("child21");
    child2.childs.push_back(child21);
    
    root.childs.push_back(child1);
    root.childs.push_back(child2);
    
    print(root);
}