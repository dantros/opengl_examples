#define BOOST_TEST_MODULE const_string test
#include <boost/test/unit_test.hpp>

template <typename T>
T square(T&& t)
{
    return t*t;
}

BOOST_AUTO_TEST_CASE( constructors_test )
{
  BOOST_TEST(square(2) == 4);
  BOOST_TEST(square(2.0) == 4.0);
  BOOST_TEST(square(2.0f) == 4.0f);
  BOOST_TEST(square(1e10) == 1e20);
}