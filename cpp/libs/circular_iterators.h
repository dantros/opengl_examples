#pragma once

// TODO: rename this to another namespace. Ex: util?, std2?.
namespace std
{

template <typename Iterator>
Iterator circular_next(Iterator const current, Iterator const begin, Iterator const end)
{
    auto next = std::next(current);
    if (next == end)
        return begin;

    return next;
}

template <typename Iterator>
Iterator circular_prev(Iterator const current, Iterator const begin, Iterator const end)
{
    if (current == begin)
        return std::prev(end);

    return std::prev(current);
}

}