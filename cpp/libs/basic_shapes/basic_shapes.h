#pragma once

#include <string>
#include "libs/shapes/shape.h"

namespace BasicShapes
{

using Shape = Shapes::Shape;
using Coord = float;
using Index = unsigned int;

Shape createRainbowTriangle();

Shape createRainbowQuad();

Shape createColorQuad(Coord r, Coord g, Coord b);

Shape createTextureQuad(std::string const& texture, Coord nx=1, Coord ny=1);

Shape createColorCircle(unsigned int steps, Coord r, Coord g, Coord b);

Shape createAxis(Coord length = 1);

Shape createGridXY(unsigned int Nx, unsigned int Ny, Coord z=0, Coord r=0, Coord g=0, Coord b=0);

Shape createRainbowCube();

Shape createColorCube(Coord r, Coord g, Coord b);

Shape createRainbowNormalsCube();

Shape createColorNormalsCube(Coord r, Coord g, Coord b);

}