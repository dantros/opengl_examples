
#include "Point.h"
#include "aabbox.h"

namespace Shapes
{

AABBox::AABBox(Point const& point) :
    x(point.x), y(point.y), z(point.z)
{}

AABBox::AABBox(Slab const& x_, Slab const& y_, Slab const& z_) :
    x(x_), y(y_), z(z_)
{}

bool AABBox::enclose(Point const& point)
{
    bool xModified = x.enclose(point.x);
    bool yModified = y.enclose(point.y);
    bool zModified = z.enclose(point.z);

    return xModified or yModified or zModified;
}

bool AABBox::contains(Point const& point) const
{
    return x.contains(point.x) and y.contains(point.y) and z.contains(point.z);
}

AABBox join(AABBox const& rhs, AABBox const& lhs)
{
    Slab const x = join(rhs.x, lhs.x);
    Slab const y = join(rhs.y, lhs.y);
    Slab const z = join(rhs.z, lhs.z);

    return {x,y,z};
}

} // Shapes