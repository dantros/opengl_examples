
#include "triangle.h"

namespace Shapes
{

AABBox boundingBox(Triangle const& triangle)
{
    AABBox bbox(triangle.a);
    bbox.enclose(triangle.b);
    bbox.enclose(triangle.c);

    return bbox;
}

} // Shapes