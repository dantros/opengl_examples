#pragma once

#include "slab.h"

namespace Shapes
{

struct AABBox
{
    Slab x,y,z;

    AABBox(Point const& point);

    AABBox(Slab const& x_, Slab const& y_, Slab const& z_);

    bool enclose(Point const& point);

    bool contains(Point const& point) const;
};

AABBox join(AABBox const& rhs, AABBox const& lhs);

} // Shapes