#pragma once

#include "types.h"

namespace Shapes
{

struct Point
{
    Coord x,y,z;
};

} // Shapes