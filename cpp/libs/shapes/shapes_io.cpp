
#include "shapes_io.h"

//namespace Shapes
//{

std::ostream& operator<<(std::ostream& os, Shapes::Point const& point)
{
    os << "("
        << point.x << ", "
        << point.y << ", "
        << point.z << ")";

    return os;
}

std::ostream& operator<<(std::ostream& os, Shapes::Triangle const& triangle)
{
    os << "["
        << triangle.a << ", "
        << triangle.b << ", "
        << triangle.c << "]";

    return os;
}

std::ostream& operator<<(std::ostream& os, Shapes::Slab const& slab)
{
    os << "["
        << slab.min << ", "
        << slab.max << "]";

    return os;
}

std::ostream& operator<<(std::ostream& os, Shapes::AABBox const& aabbox)
{
    os << "{"
        << aabbox.x << ", "
        << aabbox.y << ", "
        << aabbox.z << "}";

    return os;
}

namespace
{
    template <typename ValueT>
    std::ostream& to_ostream(std::ostream& os, std::vector<ValueT> const& values)
    {
        os << "[";
        
        auto valueIt = values.begin();
        while (valueIt != values.end())
        {
            auto& value = *valueIt;
            os << value;
            valueIt++;

            if (valueIt != values.end())
                os << ", ";
        }
        
        os << "]";

        return os;
    }
}

std::ostream& operator<<(std::ostream& os, Shapes::Vertices const& vertices)
{
    return to_ostream(os, vertices);
}

std::ostream& operator<<(std::ostream& os, Shapes::Indices const& indices)
{
    return to_ostream(os, indices);
}

std::ostream& operator<<(std::ostream& os, Shapes::Shape const& shape)
{
    os << "{ vertices: " << shape.vertices
       << ", indices: " << shape.indices << "}";

    return os;
}

//} // Shapes