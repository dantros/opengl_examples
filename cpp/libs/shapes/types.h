#pragma once

#include <glad/glad.h>
#include <cstddef>

namespace Shapes
{

using Coord = GLfloat;
using Index = GLuint;

} // Shapes