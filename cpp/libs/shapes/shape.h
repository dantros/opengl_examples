#pragma once

#include <vector>
#include <string>

#include "types.h"

namespace Shapes
{

using Vertices = std::vector<Coord>;
using Indices = std::vector<Index>;

struct Shape
{
    Vertices vertices;
    Indices indices;
    std::string texture = "";
    std::size_t stride;

    Shape(std::size_t stride_) : stride(stride_){}
};

Shape join(Shape const& rhs, Shape const& lhs);

} // Shapes