#pragma once

#include "point.h"
#include "aabbox.h"

namespace Shapes
{

struct Triangle
{
    Point a,b,c;
};

AABBox boundingBox(Triangle const& triangle);

} // Shapes
