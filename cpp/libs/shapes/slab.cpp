#pragma once

#include <algorithm>

#include "types.h"
#include "slab.h"

namespace Shapes
{

Slab::Slab(Coord coord) :
    min(coord), max(coord)
{}

Slab::Slab(Coord min_, Coord max_) :
    min(min_), max(max_)
{}

bool Slab::enclose(Coord const& coord)
{
    if (coord < min)
    {
        min = coord;
        return true;
    }
    else if (coord > max)
    {
        max = coord;
        return true;
    }
    else
    {
        return false;
    }
}

bool Slab::contains(Coord const& coord) const
{
    return (min < coord and coord < max);
}

Slab join(Slab const& rhs, Slab const& lhs)
{
    Coord const min = std::min(rhs.min, lhs.min);
    Coord const max = std::min(rhs.max, lhs.max);

    return {min, max};
}

} // Shapes