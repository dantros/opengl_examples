#pragma once

#include "types.h"

namespace Shapes
{

struct Slab
{
    Coord min, max;

    Slab(Coord coord);

    Slab(Coord min_, Coord max_);

    bool enclose(Coord const& coord);

    bool contains(Coord const& coord) const;
};

Slab join(Slab const& rhs, Slab const& lhs);

} // Shapes