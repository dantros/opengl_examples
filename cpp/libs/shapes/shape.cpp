#pragma once

#include "shape.h"

namespace Shapes
{

Shape join(Shape const& rhs, Shape const& lhs)
{
    Shape shape(rhs);

    Index offset = shape.indices.size();

    for (auto const& vertex : lhs.vertices)
        shape.vertices.push_back(vertex);
    
    for (auto const& index : lhs.indices)
        shape.indices.push_back(offset + index);

    return shape;    
}

} // Shapes