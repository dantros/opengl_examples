#pragma once

#include <iostream>
#include "point.h"
#include "triangle.h"
#include "slab.h"
#include "aabbox.h"
#include "shape.h"

//namespace Shapes
//{

std::ostream& operator<<(std::ostream& os, Shapes::Point const& point);

std::ostream& operator<<(std::ostream& os, Shapes::Triangle const& triangle);

std::ostream& operator<<(std::ostream& os, Shapes::Slab const& slab);

std::ostream& operator<<(std::ostream& os, Shapes::AABBox const& aabbox);

std::ostream& operator<<(std::ostream& os, Shapes::Vertices const& vertices);

std::ostream& operator<<(std::ostream& os, Shapes::Indices const& indices);

std::ostream& operator<<(std::ostream& os, Shapes::Shape const& shape);

//} // Shapes