#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <optional>

#include "libs/easy_shaders/gpu_shape.h"
#include "libs/shapes/shape.h"

#include "transformations.h"

namespace Transformations
{

struct SceneGraphNode;

using GPUShapePtr = std::shared_ptr<EasyShaders::GPUShape>;
using SceneGraphNodePtr = std::shared_ptr<SceneGraphNode>;

class SceneGraphNode
{
public:
    std::string name;
    Matrix4f transform;
    std::optional<GPUShapePtr> gpuShapeMaybe;
    std::vector<SceneGraphNodePtr> childs;
    
    SceneGraphNode(
        std::string const& name_) : 
        name(name_),
        transform(identity()),
        gpuShapeMaybe(std::nullopt),
        childs()
    {}

    SceneGraphNode(
        std::string const& name_,
        Matrix4f const& transform_) : 
        name(name_),
        transform(transform_),
        gpuShapeMaybe(std::nullopt),
        childs()
    {}

    SceneGraphNode(
        std::string const& name_,
        Matrix4f const& transform_,
        GPUShapePtr gpuShapePtr_) : 
        name(name_),
        transform(transform_),
        gpuShapeMaybe(gpuShapePtr_),
        childs()
    {}
};

inline std::optional<SceneGraphNodePtr> findNode(
    SceneGraphNodePtr nodePtr,
    std::string const& name)
{
    // This is the requested node
    if (nodePtr->name == name)
        return nodePtr;

    // No child of this node had the requested name
    for (auto& childPtr : nodePtr->childs)
    {
        auto nodeMaybe = findNode(childPtr, name);
        if (nodeMaybe.has_value())
            return nodeMaybe.value();
    }

    // No child of this node had the requested name
    return std::nullopt;
}

inline std::optional<Matrix4f> findTransform(
    SceneGraphNodePtr nodePtr,
    std::string const& name,
    Matrix4f const& parentTransform = identity())
{
    Matrix4f newTransform = parentTransform * nodePtr->transform;

    // This is the requested node
    if (nodePtr->name == name)
        return newTransform;
    
    // All childs are checked for the requested name
    for (auto const& child : nodePtr->childs)
    {
        auto foundTransformMaybe = findTransform(child, name, newTransform);
        if (foundTransformMaybe.has_value())
            return foundTransformMaybe.value();
    }
    
    // No child of this node had the requested name
    return std::nullopt;
}

inline std::optional<Vector4f> findPosition(
    SceneGraphNodePtr nodePtr,
    std::string const& name,
    Matrix4f const& parentTransform = identity())
{
    auto transformMaybe = findTransform(nodePtr, name, parentTransform);

    if (not transformMaybe.has_value())
        return std::nullopt;

    auto& transform = transformMaybe.value();
    return transform * Vector4f(0,0,0,1);
}

template <typename PipelineType>
void drawSceneGraphNode(
    SceneGraphNodePtr nodePtr,
    PipelineType const& pipeline,
    std::string const& transformName,
    Matrix4f const& parentTransform = identity())
{
    // Composing the transformations through this path
    Matrix4f newTransform = parentTransform * nodePtr->transform;

    if (nodePtr->gpuShapeMaybe.has_value())
    {
        auto const shapePtr = nodePtr->gpuShapeMaybe.value();
        auto const& shape = *shapePtr;
        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, transformName.c_str()), 1, GL_FALSE, newTransform.data());
        pipeline.drawShape(shape);
    }

    // If the child node is not a leaf, it MUST be a SceneGraphNode,
    // so this draw function is called recursively
    for (auto childPtr : nodePtr->childs)
        drawSceneGraphNode(childPtr, pipeline, transformName, newTransform);
}

} // Transformations