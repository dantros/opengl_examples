#pragma once

#include <Eigen/Core>

namespace Transformations
{

using Coord = float;

// Special types to prevent eigen alignment problem

//using Matrix4f = Eigen::Matrix4f;
//using Vector3f = Eigen::Vector3f;

using Matrix4f = Eigen::Matrix<Coord,4,4,Eigen::DontAlign>;
using Vector3f = Eigen::Matrix<Coord,3,1,Eigen::DontAlign>;
using Vector4f = Eigen::Matrix<Coord,4,1,Eigen::DontAlign>;

Coord constexpr PI = 3.14159265358979323846;

Matrix4f identity();

Matrix4f uniformScale(Coord s);

Matrix4f scale(Coord sx, Coord sy, Coord sz);

Matrix4f rotationX(Coord theta_radians);

Matrix4f rotationY(Coord theta_radians);

Matrix4f rotationZ(Coord theta_radians);

Matrix4f rotationA(Coord theta_radians, Vector3f axis);

Matrix4f translate(Coord tx, Coord ty, Coord tz);

Matrix4f shearing(Coord xy, Coord yx, Coord xz, Coord zx, Coord yz, Coord zy);

Matrix4f frustum(Coord left, Coord right, Coord bottom, Coord top, Coord near, Coord far);

Matrix4f perspective(Coord fovy, Coord aspect, Coord near, Coord far);

Matrix4f ortho(Coord left, Coord right, Coord bottom, Coord top, Coord near, Coord far);

Matrix4f lookAt(Vector3f const& eye, Vector3f const& at, Vector3f const& up);

} // Transformations