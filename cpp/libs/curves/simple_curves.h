#pragma once

#include <libs/simple_eigen.h>

namespace Curves
{

inline Matrix34f hermiteMatrix(
    Vector3f p1,
    Vector3f p2,
    Vector3f t1,
    Vector3f t2)
{
    Matrix34f G;
    G.col(0) << p1;
    G.col(1) << p2;
    G.col(2) << t1;
    G.col(3) << t2;

    Matrix4f Mh;
    Mh << 1, 0, -3, 2,
        0, 0, 3, -2,
        0, 1, -2, 1,
        0, 0, -1, 1;

    return G * Mh;
}

inline Matrix34f bezierMatrix(
    Vector3f p0,
    Vector3f p1,
    Vector3f p2,
    Vector3f p3)
{
    Matrix34f G;
    G.col(0) << p0;
    G.col(1) << p1;
    G.col(2) << p2;
    G.col(3) << p3;

    Matrix4f Mb;
    Mb << 1, -3, 3, -1,
        0, 3, -6, 3,
        0, 0, 3, -3,
        0, 0, 0, 1;

    return G * Mb;
}

inline Matrix34f catmullRomMatrix(
    Vector3f p_im1,
    Vector3f p_i,
    Vector3f p_ip1,
    Vector3f p_ip2)
{
    Matrix34f G;
    G.col(0) << p_im1;
    G.col(1) << p_i;
    G.col(2) << p_ip1;
    G.col(3) << p_ip2;

    Matrix4f Mh;
    Mh << 0, -1, 2, -1,
        2, 0, -5, 3,
        0, 1, 4, -3,
        0, 0, -1, 1;
    Mh = 0.5 * Mh;

    return G * Mh;
}

inline Vector4f generateT(Coord t)
{
    Coord const t2 = t * t;
    
    Vector4f out;
    out << 1, t, t2, t2*t;

    return out;
}

}