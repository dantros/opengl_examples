#pragma once

#include <array>
#include <vector>
#include <initializer_list>
#include <optional>
#include <algorithm>

#include <libs/simple_eigen.h>
#include <libs/circular_iterators.h>

namespace Mesh
{

using Index = std::size_t;

using TriangleConnection = std::array<Index, 3>;
using QuadConnection = std::array<Index, 4>;
using PolygonConnection = std::vector<Index>;

struct PositionVertex
{
    Vector3f position;
};

struct ColorVertex
{
    Vector3f position;
    Vector4f color;
};

struct TextureVertex
{
    Vector3f position;
    Vector2f textureCoordinates;
};

struct NormalVertex
{
    Vector3f position, normal;
};

struct ColorNormalVertex
{
    Vector3f position;
    Vector4f color;
    Vector3f normal;
};

struct TextureNormalVertex
{
    Vector3f position;
    Vector2f textureCoordinates;
    Vector3f normal;
};

template <typename T>
struct has_normals;

template <>
struct has_normals<PositionVertex>
{
    static const bool value = false;
};

template <>
struct has_normals<ColorVertex>
{
    static const bool value = false;
};

template <>
struct has_normals<TextureVertex>
{
    static const bool value = false;
};

template <>
struct has_normals<NormalVertex>
{
    static const bool value = true;
};

template <>
struct has_normals<ColorNormalVertex>
{
    static const bool value = true;
};

template <>
struct has_normals<TextureNormalVertex>
{
    static const bool value = true;
};

template <typename T>
struct vertex_normal_type;

template <>
struct vertex_normal_type<PositionVertex>
{
    using type = NormalVertex;
};

template <>
struct vertex_normal_type<ColorVertex>
{
    using type = ColorNormalVertex;
};

template <>
struct vertex_normal_type<TextureVertex>
{
    using type = TextureNormalVertex;
};

inline
NormalVertex setNormal(PositionVertex const& original, Vector3f const& normal)
{
    NormalVertex newVertex;
    newVertex.position = original.position;
    newVertex.normal = normal;

    return newVertex;
}

inline
ColorNormalVertex setNormal(ColorVertex const& original, Vector3f const& normal)
{
    ColorNormalVertex newVertex;
    newVertex.position = original.position;
    newVertex.color = original.color;
    newVertex.normal = normal;

    return newVertex;
}

inline
TextureNormalVertex setNormal(TextureVertex const& original, Vector3f const& normal)
{
    TextureNormalVertex newVertex;
    newVertex.position = original.position;
    newVertex.textureCoordinates = original.textureCoordinates;
    newVertex.normal = normal;

    return newVertex;
}

template <typename VertexType, typename ConnectionType>
struct Mesh
{
    using Vertices = std::vector<VertexType>;
    using Connections = std::vector<ConnectionType>;
    
    Vertices vertices;
    Connections shapeConnections;

    Index insertVertex(VertexType const& vertex)
    {
        vertices.push_back(vertex);
        return vertices.size() - 1;
    }

    Index insertVertex(VertexType&& vertex)
    {
        vertices.emplace_back(std::move(vertex));
        return vertices.size() - 1;
    }

    void insertConnection(std::initializer_list<Index> indices)
    {
        shapeConnections.emplace_back(indices);
    }
};

template <typename VertexType>
using TriangleMesh = Mesh<VertexType, TriangleConnection>;

template <typename VertexType>
using QuadMesh = Mesh<VertexType, QuadConnection>;

template <typename VertexType>
using PolygonMesh = Mesh<VertexType, PolygonConnection>;


struct Face
{
    Face() : vertices(){}

    Face(std::size_t numberOfVertices) : vertices()
    {
        vertices.reserve(numberOfVertices);
    }

    Face(std::initializer_list<Vector3f> vertices) : vertices(vertices)
    {
        // TODO: this should be a debug assert only
        //assert(isValid());
    }

    bool isPlane() const
    {
        // TODO: Add check that all vertices belong to the same plane.
        return true;
    }

    bool isValid() const
    {
        // TODO: cehck for no self intersections
        return vertices.size() >= 3 and isPlane();
    }

    std::vector<Vector3f> vertices;
};

inline bool operator==(Face const& lhs, Face const& rhs)
{
    return lhs.vertices == rhs.vertices;
}

inline bool equivalent(Face const& lhs, Face const& rhs)
{
    if (lhs.vertices.size() != rhs.vertices.size())
        return false;

    // Finding anchor point
    auto const& lhsFirstValue = lhs.vertices.front();
    auto const rhsEquivIt = std::find(rhs.vertices.begin(), rhs.vertices.end(), lhsFirstValue);

    if (rhsEquivIt == rhs.vertices.end())
        return false;

    auto rhsIt = circular_next(rhsEquivIt, rhs.vertices.begin(), rhs.vertices.end());

    auto lhsIt = circular_next(lhs.vertices.begin(), lhs.vertices.begin(), lhs.vertices.end());

    // Both values will be the same when the whole round has been checked.
    while (rhsIt != rhsEquivIt)
    {
        auto& lhsValue = *lhsIt;
        auto& rhsValue = *rhsIt;

        if (lhsValue != rhsValue)
            return false;

        // moving iterators to the next element
        lhsIt = std::circular_next(lhsIt, lhs.vertices.begin(), lhs.vertices.end());
        rhsIt = std::circular_next(rhsIt, rhs.vertices.begin(), rhs.vertices.end());
    }
    
    return true;
}

template <typename VertexType, typename ConnectionType>
Face makeFace(Mesh<VertexType, ConnectionType> const& mesh, Index faceIndex)
{
    auto const& faceConnection = mesh.shapeConnections.at(faceIndex);

    std::size_t const numberOfVertices = faceConnection.size();
    Face face(numberOfVertices);

    for (auto const& index : faceConnection)
    {
        VertexType const& vertex = mesh.vertices.at(index);
        Vector3f const& position = vertex.position;
        face.vertices.push_back(position);
    }

    // TODO: this should be a debug assert only
    assert(face.isValid());

    return face;
}

inline Vector3f computeNormal(Face const& face)
{
    // TODO: this should be a debug assert only
    assert(face.isValid());

    auto const& v0 = face.vertices[0];
    auto const& v1 = face.vertices[1];
    auto const& v2 = face.vertices[2];

    auto const d1 = v1 - v0;
    auto const d2 = v2 - v0;
    auto n = d1.cross(d2);
    n.normalize();

    return n;
}

template <typename VertexType, typename ConnectionType>
Vector3f computeNormal(Mesh<VertexType, ConnectionType> const& mesh, Index faceIndex)
{
    /* vertex type should not have normals already */
    static_assert(not has_normals<VertexType>::value, "VertexType already has normals");

    Face const face = makeFace(mesh, faceIndex);
    Vector3f const normal = computeNormal(face);

    return normal;
}

template <typename VertexType, typename ConnectionType>
std::vector<Vector3f> computeNormalsPerFace(Mesh<VertexType, ConnectionType> const& mesh)
{
    /* vertex type should not have normals already */
    static_assert(not has_normals<VertexType>::value, "VertexType already has normals");

    auto const numberOfFaces =  mesh.shapeConnections.size();

    std::vector<Vector3f> normals;
    normals.reserve(numberOfFaces);

    for (Index faceIndex = 0; faceIndex < numberOfFaces; faceIndex++)
    {
        Vector3f normal = computeNormal(mesh, faceIndex);
        normals.push_back(normal); 
    }

    // We should have a 1 to 1 correspondence
    assert(normals.size() == mesh.shapeConnections.size());

    return normals;
}

inline
Vector3f average(std::vector<Vector3f> const& vectors)
{
    std::size_t N = vectors.size();

    Vector3f average(0,0,0);

    for (auto const& vector :  vectors)
    {
        average += vector / N;
    }

    return average;
}

inline
Vector3f average(std::vector<Vector3f> const& vectors, std::vector<Index> const& indices)
{
    std::size_t N = indices.size();

    Vector3f average(0,0,0);

    for (auto const& index : indices)
    {
        auto const& vector = vectors.at(index);
        average += vector / N;
    }

    return average;
}

#if 0

template <typename VertexType, typename ConnectionType>
std::vector<std::vector<Index>> extractVertexToFacesRelationship(
    Mesh<VertexType, ConnectionType> const& mesh)
{
    // Each vertex may be associated with an arbitrary number of faces.
    // Here we store indices to all faces associated to each vertex index
    // Ex: 
    std::size_t numberOfFaces = mesh.shapeConnections.size();

    // Default initialization with numberOfFaces elements
    std::vector<std::vector<Index>> facesPerVertex(numberOfFaces);

    for (Index faceIndex = 0; faceIndex < numberOfFaces; faceIndex++)
    {
        auto const& connection = mesh.shapeConnections[faceIndex];
        for (auto const& vertexIndex : connection)
        {
            auto& vertexNormals = facesPerVertex.at(vertexIndex);
            vertexNormals.push_back(faceIndex);
        }
    }

    return facesPerVertex;
}

template <typename VertexType, typename ConnectionType>
auto computeNormalsPerVertex(Mesh<VertexType, ConnectionType> const& mesh)
{
    /* vertex type should not have normals already */
    static_assert(not has_normals<VertexType>::value, "VertexType already has normals");

    using VertexNormalType = typename vertex_normal_type<VertexType>::type;
    
    Mesh<VertexNormalType, ConnectionType> meshWithNormals;

    // shape connection indices are directly copied
    meshWithNormals.shapeConnections = mesh.shapeConnections;

    auto const& vertexToFaces = extractVertexToFacesRelationship(mesh);
    auto const normalsPerFace = computeNormalsPerFace(mesh);

    std::size_t numberOfVertices = mesh.vertices.size();
    for (Index vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++)
    {
        auto const& originalVertex = meshWithNormals.vertices[vertexIndex];
        auto const& faceIndices = vertexToFaces.at(vertexIndex);
        Vector3f vertexNormal = averageIndexedNormals(normalsPerFace, faceIndices);

        VertexNormalType const newVertex = setNormal(originalVertex, vertexNormal);
        meshWithNormals.insertVertex(newVertex);
    }

    return meshWithNormals;
}

#endif

/*
TriangleMesh toTriangleMesh(QuadMesh const& quadMesh)
{

}

TriangleMesh toTriangleMesh(PolygonMesh const& polygonMesh)
{
    
}
*/
} // namespace Mesh