#define BOOST_TEST_MODULE MeshUnitTests

#include <boost/test/unit_test.hpp>
#include <vector>
#include <iostream>
#include <cmath>
#include <libs/simple_eigen.h>
#include "../mesh.h"
#include "../mesh_io.h"

// TODO: to move this to the circular iterators libs tests
BOOST_AUTO_TEST_SUITE( CircularIterationSuite )

BOOST_AUTO_TEST_CASE( CircularNextTest )
{
    std::vector<int> v{2,3,4,1};

    auto it = v.begin();
    BOOST_TEST( *it == 2);

    it = std::circular_next(it, v.begin(), v.end());
    BOOST_TEST( *it == 3);

    it = std::circular_next(it, v.begin(), v.end());
    BOOST_TEST( *it == 4);

    it = std::circular_next(it, v.begin(), v.end());
    BOOST_TEST( *it == 1);

    it = std::circular_next(it, v.begin(), v.end());
    BOOST_TEST( *it == 2);

    it = std::circular_next(it, v.begin(), v.end());
    BOOST_TEST( *it == 3);
    
    it = std::circular_next(it, v.begin(), v.end());
    BOOST_TEST( *it == 4);

    it = std::circular_next(it, v.begin(), v.end());
    BOOST_TEST( *it == 1);
}

BOOST_AUTO_TEST_CASE( CircularPrevTest )
{
    std::vector<int> v{2,3,4,1};

    auto it = v.begin();
    BOOST_TEST( *it == 2);

    it = std::circular_prev(it, v.begin(), v.end());
    BOOST_TEST( *it == 1);

    it = std::circular_prev(it, v.begin(), v.end());
    BOOST_TEST( *it == 4);

    it = std::circular_prev(it, v.begin(), v.end());
    BOOST_TEST( *it == 3);

    it = std::circular_prev(it, v.begin(), v.end());
    BOOST_TEST( *it == 2);

    it = std::circular_prev(it, v.begin(), v.end());
    BOOST_TEST( *it == 1);
    
    it = std::circular_prev(it, v.begin(), v.end());
    BOOST_TEST( *it == 4);

    it = std::circular_prev(it, v.begin(), v.end());
    BOOST_TEST( *it == 3);
}

BOOST_AUTO_TEST_SUITE_END()

bool operator==(Vector3f const& lhs, Vector3f const& rhs)
{
    double constexpr epsilon = 1e-4;
    return (lhs - rhs).norm() < epsilon;
}

BOOST_AUTO_TEST_SUITE( FaceSuite )

BOOST_AUTO_TEST_CASE( PointTest )
{
    Mesh::Face face({{0,0,0}});

    BOOST_CHECK(not face.isValid());
}

BOOST_AUTO_TEST_CASE( SegmentTest )
{
    Mesh::Face face({{0,0,0},{1,0,0}});

    BOOST_CHECK(not face.isValid());
}

BOOST_AUTO_TEST_CASE( TriangleTest )
{
    Mesh::Face face({{0,0,0},{1,0,0},{1,1,0}});

    BOOST_CHECK(face.isValid());
    BOOST_CHECK(face.isPlane());
}

BOOST_AUTO_TEST_SUITE_END()

using SimpleMesh = Mesh::PolygonMesh<Mesh::PositionVertex>;

SimpleMesh generatePiramid()
{
    SimpleMesh mesh;

	auto const top = mesh.insertVertex({{0,0,1}});
    auto const xpyp = mesh.insertVertex({{1,1,0}});
    auto const xpym = mesh.insertVertex({{1,-1,0}});
    auto const xmyp = mesh.insertVertex({{-1,1,0}});
    auto const xmym = mesh.insertVertex({{-1,-1,0}});

    mesh.insertConnection({xpym, xpyp, top});
    mesh.insertConnection({xpyp, xmyp, top});
    mesh.insertConnection({xmyp, xmym, top});
    mesh.insertConnection({xmym, xpym, top});
    mesh.insertConnection({xpyp, xpym, xmym, xmyp});

    return mesh;
}

BOOST_AUTO_TEST_SUITE( MeshSuite )

BOOST_AUTO_TEST_CASE( PiramidTest )
{
    auto const piramidMesh = generatePiramid();
    //std::cout << piramidMesh << std::endl;

    BOOST_CHECK(true);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE( MakeFaceSuite )

BOOST_AUTO_TEST_CASE( PiramidTest )
{
    using namespace Mesh;

    auto const piramidMesh = generatePiramid();

    Face const face0 = makeFace(piramidMesh, 0);
    Face const golden0({{1,-1,0},{1,1,0},{0,0,1}});
    BOOST_TEST(equivalent(face0, golden0));

    Face const face1 = makeFace(piramidMesh, 1);
    Face const golden1({{1,1,0},{-1,1,0},{0,0,1}});
    BOOST_TEST(equivalent(face1, golden1));

    Face const face2 = makeFace(piramidMesh, 2);
    Face const golden2({{-1,1,0},{-1,-1,0},{0,0,1}});
    BOOST_TEST(equivalent(face2, golden2));

    Face const face3 = makeFace(piramidMesh, 3);
    Face const golden3({{-1,-1,0},{1,-1,0},{0,0,1}});
    BOOST_TEST(equivalent(face3, golden3));

    Face const face4 = makeFace(piramidMesh, 4);
    Face const golden4({{1,-1,0},{-1,-1,0},{-1,1,0},{1,1,0}});
    BOOST_TEST(equivalent(face3, golden3));
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE( ComputingNormalsOnFacesSuite )

BOOST_AUTO_TEST_CASE( PiramidTest )
{
    using namespace Mesh;

    auto const piramidMesh = generatePiramid();

    double constexpr epsilon = 1e-4;

    Face const face0 = makeFace(piramidMesh, 0);
    Vector3f const normal0 = computeNormal(face0);
    Vector3f const golden0(0.707107, 0, 0.707107);
    BOOST_TEST((normal0 - golden0).norm() < epsilon);

    Face const face1 = makeFace(piramidMesh, 1);
    Vector3f const normal1 = computeNormal(face1);
    Vector3f const golden1(0, 0.707107, 0.707107);
    BOOST_TEST((normal1 - golden1).norm() < epsilon);

    Face const face2 = makeFace(piramidMesh, 2);
    Vector3f const normal2 = computeNormal(face2);
    Vector3f const golden2(-0.707107, 0, 0.707107);
    BOOST_TEST((normal2 - golden2).norm() < epsilon);

    Face const face3 = makeFace(piramidMesh, 3);
    Vector3f const normal3 = computeNormal(face3);
    Vector3f const golden3(0, -0.707107, 0.707107);
    BOOST_TEST((normal3 - golden3).norm() < epsilon);

    Face const face4 = makeFace(piramidMesh, 4);
    Vector3f const normal4 = computeNormal(face4);
    Vector3f const golden4(0, 0, -1);
    BOOST_TEST((normal4 - golden4).norm() < epsilon);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE( ComputingNormalsPerFacesSuite )

BOOST_AUTO_TEST_CASE( PiramidTest )
{
    using namespace Mesh;

    auto const piramidMesh = generatePiramid();

    double constexpr epsilon = 1e-4;

    Vector3f const normal0 = computeNormal(piramidMesh, 0);
    Vector3f const golden0(0.707107, 0, 0.707107);
    BOOST_TEST((normal0 - golden0).norm() < epsilon);

    Vector3f const normal1 = computeNormal(piramidMesh, 1);
    Vector3f const golden1(0, 0.707107, 0.707107);
    BOOST_TEST((normal1 - golden1).norm() < epsilon);

    Vector3f const normal2 = computeNormal(piramidMesh, 2);
    Vector3f const golden2(-0.707107, 0, 0.707107);
    BOOST_TEST((normal2 - golden2).norm() < epsilon);

    Vector3f const normal3 = computeNormal(piramidMesh, 3);
    Vector3f const golden3(0, -0.707107, 0.707107);
    BOOST_TEST((normal3 - golden3).norm() < epsilon);

    Vector3f const normal4 = computeNormal(piramidMesh, 4);
    Vector3f const golden4(0, 0, -1);
    BOOST_TEST((normal4 - golden4).norm() < epsilon);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE( ComputingNormalsPerFaceOnMeshSuite )

BOOST_AUTO_TEST_CASE( PiramidTest )
{
    using namespace Mesh;

    auto const piramidMesh = generatePiramid();

    double constexpr epsilon = 1e-4;

    auto const normals = computeNormalsPerFace(piramidMesh);

    Vector3f const golden0(0.707107, 0, 0.707107);
    BOOST_TEST((normals[0] - golden0).norm() < epsilon);

    Vector3f const golden1(0, 0.707107, 0.707107);
    BOOST_TEST((normals[1] - golden1).norm() < epsilon);

    Vector3f const golden2(-0.707107, 0, 0.707107);
    BOOST_TEST((normals[2] - golden2).norm() < epsilon);

    Vector3f const golden3(0, -0.707107, 0.707107);
    BOOST_TEST((normals[3] - golden3).norm() < epsilon);

    Vector3f const golden4(0, 0, -1);
    BOOST_TEST((normals[4] - golden4).norm() < epsilon);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE( AverageVectorsSuite )

BOOST_AUTO_TEST_CASE( AllVectorsSimpleTest )
{
    using namespace Mesh;

    Vector3f averaged0 = average({{1,0,0},{-1,0,0}});
    Vector3f golden0(0,0,0);
    BOOST_TEST(averaged0 == golden0);

    BOOST_TEST(average({{0,1,0},{0,-1,0}}) == Vector3f(0,0,0));
    BOOST_TEST(average({{0,0,0},{2,0,0}}) == Vector3f(1,0,0));
    BOOST_TEST(average({{1,1,0},{-1,1,0}}) == Vector3f(0,1,0));
    BOOST_TEST(average({{1,1,0},{-1,1,0},{-1,-1,0},{1,-1,0}}) == Vector3f(0,0,0));
    BOOST_TEST(average({{1,1,0},{-1,1,0},{2,1,0},{-2,1,0}}) == Vector3f(0,1,0));
    BOOST_TEST(average({{1,1,0},{-1,1,0},{2,1,0},{4,1,0}}) == Vector3f(1.5,1,0));
}

BOOST_AUTO_TEST_CASE( IndexedVectorsSimpleTest )
{
    using namespace Mesh;

    std::vector<Vector3f> vectors
    {
        {1,0,0},{-1,0,0},{0,1,0},{0,-1,0},{0,0,2},{0,0,-2}
    };

    BOOST_TEST(average(vectors,{0,1}) == Vector3f(0,0,0));
    BOOST_TEST(average(vectors,{2,3}) == Vector3f(0,0,0));
    BOOST_TEST(average(vectors,{4,5}) == Vector3f(0,0,0));
    BOOST_TEST(average(vectors,{0,1,2,3}) == Vector3f(0,0,0));
    BOOST_TEST(average(vectors,{0,1,2,3,4,5}) == Vector3f(0,0,0));
    BOOST_TEST(average(vectors,{0,0,0}) == Vector3f(1,0,0));
    BOOST_TEST(average(vectors,{4,4,1,1}) == Vector3f(-0.5,0,1));
    BOOST_TEST(average(vectors,{0,2}) == Vector3f(0.5,0.5,0));
}

BOOST_AUTO_TEST_SUITE_END()

