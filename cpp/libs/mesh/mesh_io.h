#pragma once

#include "mesh.h"

namespace Mesh
{

inline
std::ostream& operator<<(std::ostream& os, Vector2f const& vertex)
{
    os << vertex[0] << ", "
        << vertex[1];
    
    return os;
}

inline
std::ostream& operator<<(std::ostream& os, Vector3f const& vertex)
{
    os << vertex[0] << ", "
        << vertex[1] << ", "
        << vertex[2];
    
    return os;
}

inline
std::ostream& operator<<(std::ostream& os, Vector4f const& vertex)
{
    os << vertex[0] << ", "
        << vertex[1] << ", "
        << vertex[2] << ", "
        << vertex[3];
    
    return os;
}

inline
std::ostream& operator<<(std::ostream& os, PositionVertex const& vertex)
{
    os << "p(" << vertex.position << ")";
    
    return os;
}

inline
std::ostream& operator<<(std::ostream& os, ColorVertex const& vertex)
{
    os << "p(" << vertex << ") "
        << "c(" << vertex.color << ")";
    
    return os;
}

inline
std::ostream& operator<<(std::ostream& os, TextureVertex const& vertex)
{
    os << "p(" << vertex << ") "
        << "t(" << vertex.textureCoordinates << ")";
    
    return os;
}

inline
std::ostream& operator<<(std::ostream& os, NormalVertex const& vertex)
{
    os << "p(" << vertex << ") "
        << "n(" << vertex.normal << ")";
    
    return os;
}

inline
std::ostream& operator<<(std::ostream& os, ColorNormalVertex const& vertex)
{
    os << "p(" << vertex << ") "
        << "c(" << vertex.color << ")"
        << "n(" << vertex.normal << ")";
    
    return os;
}

inline
std::ostream& operator<<(std::ostream& os, TextureNormalVertex const& vertex)
{
    os << "p(" << vertex << ") "
        << "t(" << vertex.textureCoordinates << ")"
        << "n(" << vertex.normal << ")";
    
    return os;
}
template <typename ElementType>
std::ostream& operator<<(std::ostream& os, std::vector<ElementType> const& elements)
{
    os << "[";
    for (auto const& element : elements)
    {
        os << "(" << element << "), ";
    }
    os << "]";
    
    return os;
}

inline
std::ostream& operator<<(std::ostream& os, TriangleConnection const& triangleConnection)
{
    os << "<"
        << triangleConnection[0] << ","
        << triangleConnection[1] << ","
        << triangleConnection[2] << ">";
    
    return os;
}

inline
std::ostream& operator<<(std::ostream& os, QuadConnection const& quadConnection)
{
    os << "<"
        << quadConnection[0] << ","
        << quadConnection[1] << ","
        << quadConnection[2] << ","
        << quadConnection[3] << ">";
    
    return os;
}

inline
std::ostream& operator<<(std::ostream& os, PolygonConnection const& polygonConnection)
{
    os << "<";
    for (auto const& index : polygonConnection)
    {
        os << index << ", ";
    }
    os << ">";
    
    return os;
}

template <typename VertexType, typename ConnectionType>
std::ostream& operator<<(std::ostream& os, Mesh<VertexType, ConnectionType> const& mesh)
{
    os << "Mesh{" << std::endl;
    os << "    vertices={" << mesh.vertices << "}" << std::endl;
    os << "    shapeConnections={" << mesh.shapeConnections << "}" << std::endl;
    os << "}"<< std::endl;
    return os;
}

inline
std::ostream& operator<<(std::ostream& os, Face const& face)
{
    return os << "Face{" << face.vertices << "}";
}

} // Mesh