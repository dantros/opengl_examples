#pragma once

#include "load_shaders.h"
#include "gpu_shape.h"


namespace EasyShaders
{

struct SimpleTextureTransformShaderProgram
{
    GLuint shaderProgram;

    SimpleTextureTransformShaderProgram()
    {
        std::string const vertexShaderCode =
            "#version 130                                                         \n"
            "                                                                     \n"
            "uniform mat4 transform;                                              \n"
            "                                                                     \n"
            "in vec3 position;                                                    \n"
            "in vec2 texCoords;                                                   \n"
            "                                                                     \n"
            "out vec2 outTexCoords;                                               \n"
            "                                                                     \n"
            "void main()                                                          \n"
            "{                                                                    \n"
            "    gl_Position = transform * vec4(position, 1.0f);                  \n"
            "    outTexCoords = texCoords;                                        \n"
            "}                                                                    \n";

        std::string const fragmentShaderCode =
            "#version 130                                                         \n"
            "                                                                     \n"
            "in vec2 outTexCoords;                                                \n"
            "                                                                     \n"
            "out vec4 outColor;                                                   \n"
            "                                                                     \n"
            "uniform sampler2D samplerTex;                                        \n"
            "                                                                     \n"
            "void main()                                                          \n"
            "{                                                                    \n"
            "    outColor = texture(samplerTex, outTexCoords);                    \n"
            "}                                                                    \n";

        shaderProgram = createShaderProgramFromCode({
            {GL_VERTEX_SHADER, vertexShaderCode.c_str()},
            {GL_FRAGMENT_SHADER, fragmentShaderCode.c_str()}
        });
    }

    void drawShape(GPUShape const& gpuShape, GLuint mode=GL_TRIANGLES) const
    {
        // Binding buffers
        glBindVertexArray(gpuShape.vao);
        glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo);
        glBindTexture(GL_TEXTURE_2D, gpuShape.texture);

        // position attribute
        auto position = glGetAttribLocation(shaderProgram, "position");
        glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void*)0);
        glEnableVertexAttribArray(position);

        // color attribute
        auto color = glGetAttribLocation(shaderProgram, "texCoords");
        glVertexAttribPointer(color, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
        glEnableVertexAttribArray(color);

        // Render the active element buffer with the active shader program
        glDrawElements(mode, gpuShape.size, GL_UNSIGNED_INT, 0);

        // note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0); 

        // remember: do NOT unbind the EBO while a VAO is active as the bound element buffer object IS stored in the VAO; keep the EBO bound.
        //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
        // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
        glBindVertexArray(0); 
    }
};
}