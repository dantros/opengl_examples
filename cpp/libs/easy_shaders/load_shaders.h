#pragma once

#include <string>
#include <initializer_list>
#include <glad/glad.h>

namespace EasyShaders
{

struct ShaderCode
{
    GLenum const type;
    std::string const sourceCode;
};

struct ShaderFile
{
    GLenum const type;
    std::string const filename;
};

ShaderCode readShaderFile(GLenum type, std::string const& filename);

GLuint compileShader(GLenum type, std::string const& sourceCode);

GLuint createShaderProgramFromCode(std::initializer_list<ShaderCode> shaderCodes);

GLuint createShaderProgramFromFiles(std::initializer_list<ShaderFile> shaderFiles);

} // EasyShaders