#pragma once

#include <string>
#include "load_shaders.h"
#include "gpu_shape.h"

namespace EasyShaders
{
namespace
{
    class ColorTransformShaderProgramBase
    {
    public:
        GLuint shaderProgram;

		ColorTransformShaderProgramBase(std::string const& vertexShaderFilename, std::string const& fragmentShaderFilename) :
            shaderProgram(createShaderProgramFromFiles({
                {GL_VERTEX_SHADER, vertexShaderFilename},
                {GL_FRAGMENT_SHADER, fragmentShaderFilename}
            }))
        {}

        void drawShape(GPUShape const& gpuShape, GLuint mode=GL_TRIANGLES) const
        {
            // Binding buffers
            glBindVertexArray(gpuShape.vao);
            glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo);

            // position attribute
            auto position = glGetAttribLocation(shaderProgram, "position");
            glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (void*)0);
            glEnableVertexAttribArray(position);

            // color attribute
            auto color = glGetAttribLocation(shaderProgram, "color");
            glVertexAttribPointer(color, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
            glEnableVertexAttribArray(color);

            // Render the active element buffer with the active shader program
            glDrawElements(mode, gpuShape.size, GL_UNSIGNED_INT, 0);

            // note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
            glBindBuffer(GL_ARRAY_BUFFER, 0); 

            // remember: do NOT unbind the EBO while a VAO is active as the bound element buffer object IS stored in the VAO; keep the EBO bound.
            //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

            // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
            // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
            glBindVertexArray(0); 
        }
    };

    class TextureTransformShaderProgramBase
    {
    public:
        GLuint shaderProgram;

        TextureTransformShaderProgramBase(std::string const& vertexShaderFilename, std::string const& fragmentShaderFilename) :
            shaderProgram(createShaderProgramFromFiles({
                {GL_VERTEX_SHADER, vertexShaderFilename},
                {GL_FRAGMENT_SHADER, fragmentShaderFilename}
            }))
        {}

        void drawShape(GPUShape const& gpuShape, GLuint mode=GL_TRIANGLES) const
        {
            // Binding buffers
            glBindVertexArray(gpuShape.vao);
            glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo);
            glBindTexture(GL_TEXTURE_2D, gpuShape.texture);

            // position attribute
            auto position = glGetAttribLocation(shaderProgram, "position");
            glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void*)0);
            glEnableVertexAttribArray(position);

            // color attribute
            auto color = glGetAttribLocation(shaderProgram, "texCoords");
            glVertexAttribPointer(color, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
            glEnableVertexAttribArray(color);

            // Render the active element buffer with the active shader program
            glDrawElements(mode, gpuShape.size, GL_UNSIGNED_INT, 0);

            // note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
            glBindBuffer(GL_ARRAY_BUFFER, 0); 

            // remember: do NOT unbind the EBO while a VAO is active as the bound element buffer object IS stored in the VAO; keep the EBO bound.
            //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

            // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
            // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
            glBindVertexArray(0); 
        }
    };

} // namespace

// Colors

class ColorTransformShaderProgram : public ColorTransformShaderProgramBase
{
public:
    ColorTransformShaderProgram() : ColorTransformShaderProgramBase(
        "media/shaders/transform_vertex_shader.glsl",
        "media/shaders/transform_fragment_shader.glsl"
    )
    {}
};

class ColorModelViewProjectionShaderProgram : public ColorTransformShaderProgramBase
{
public:
    ColorModelViewProjectionShaderProgram() : ColorTransformShaderProgramBase(
        "media/shaders/model_view_projection_vertex_shader.glsl",
        "media/shaders/model_view_projection_fragment_shader.glsl"
    )
    {}
};

// Textures

class TextureTransformShaderProgram : public TextureTransformShaderProgramBase
{
public:
    TextureTransformShaderProgram() : TextureTransformShaderProgramBase(
        "media/shaders/texture_transform_vertex_shader.glsl",
        "media/shaders/texture_transform_fragment_shader.glsl"
    )
    {}
};

class TextureModelViewProjectionShaderProgram : public TextureTransformShaderProgramBase
{
public:
    TextureModelViewProjectionShaderProgram() : TextureTransformShaderProgramBase(
        "media/shaders/texture_model_view_projection_vertex_shader.glsl",
        "media/shaders/texture_model_view_projection_fragment_shader.glsl"
    )
    {}
};

namespace
{
    class ColorLightingShaderProgramBase
    {
    public:
        GLuint shaderProgram;

		ColorLightingShaderProgramBase(std::string const& vertexShaderFilename, std::string const& fragmentShaderFilename) :
            shaderProgram(createShaderProgramFromFiles({
                {GL_VERTEX_SHADER, vertexShaderFilename},
                {GL_FRAGMENT_SHADER, fragmentShaderFilename}
            }))
        {}

        void drawShape(GPUShape const& gpuShape, GLuint mode=GL_TRIANGLES) const
        {
            // Binding buffers
            glBindVertexArray(gpuShape.vao);
            glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo);

            // position attribute
            auto position = glGetAttribLocation(shaderProgram, "position");
            glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (void*)0);
            glEnableVertexAttribArray(position);

            // color attribute
            auto color = glGetAttribLocation(shaderProgram, "color");
            glVertexAttribPointer(color, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
            glEnableVertexAttribArray(color);

            // normal attribute
            auto normal = glGetAttribLocation(shaderProgram, "normal");
            glVertexAttribPointer(normal, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (void*)(6 * sizeof(GLfloat)));
            glEnableVertexAttribArray(normal);

            // Render the active element buffer with the active shader program
            glDrawElements(mode, gpuShape.size, GL_UNSIGNED_INT, 0);

            // note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
            glBindBuffer(GL_ARRAY_BUFFER, 0); 

            // remember: do NOT unbind the EBO while a VAO is active as the bound element buffer object IS stored in the VAO; keep the EBO bound.
            //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

            // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
            // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
            glBindVertexArray(0); 
        }
    };

    class LightingTextureShaderProgramBase
    {
    public:
        GLuint shaderProgram;

		LightingTextureShaderProgramBase(std::string const& vertexShaderFilename, std::string const& fragmentShaderFilename) :
            shaderProgram(createShaderProgramFromFiles({
                {GL_VERTEX_SHADER, vertexShaderFilename},
                {GL_FRAGMENT_SHADER, fragmentShaderFilename}
            }))
        {}

        void drawShape(GPUShape const& gpuShape, GLuint mode=GL_TRIANGLES) const
        {
            // Binding buffers
            glBindVertexArray(gpuShape.vao);
            glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo);
            glBindTexture(GL_TEXTURE_2D, gpuShape.texture);

            // position attribute
            auto position = glGetAttribLocation(shaderProgram, "position");
            glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (void*)0);
            glEnableVertexAttribArray(position);

            // color attribute
            auto texCoords = glGetAttribLocation(shaderProgram, "texCoords");
            glVertexAttribPointer(texCoords, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
            glEnableVertexAttribArray(texCoords);

            // normal attribute
            auto normal = glGetAttribLocation(shaderProgram, "normal");
            glVertexAttribPointer(normal, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (void*)(5 * sizeof(GLfloat)));
            glEnableVertexAttribArray(normal);

            // Render the active element buffer with the active shader program
            glDrawElements(mode, gpuShape.size, GL_UNSIGNED_INT, 0);

            // note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
            glBindBuffer(GL_ARRAY_BUFFER, 0); 

            // remember: do NOT unbind the EBO while a VAO is active as the bound element buffer object IS stored in the VAO; keep the EBO bound.
            //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

            // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
            // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
            glBindVertexArray(0); 
        }
    };
 } // namespace

 // Lighting

class SimplePhongColorShaderProgram : public ColorLightingShaderProgramBase
{
public:
    SimplePhongColorShaderProgram() : ColorLightingShaderProgramBase(
        "media/shaders/simple_phong_shader_program_vertex_shader.glsl",
        "media/shaders/simple_phong_shader_program_fragment_shader.glsl"
    )
    {}
};

class SimpleGouraudColorShaderProgram : public ColorLightingShaderProgramBase
{
public:
    SimpleGouraudColorShaderProgram() : ColorLightingShaderProgramBase(
        "media/shaders/simple_gouraud_shader_program_vertex_shader.glsl",
        "media/shaders/simple_gouraud_shader_program_fragment_shader.glsl"
    )
    {}
};

class SimpleFlatColorShaderProgram : public ColorLightingShaderProgramBase
{
public:
    SimpleFlatColorShaderProgram() : ColorLightingShaderProgramBase(
        "media/shaders/simple_flat_shader_program_vertex_shader.glsl",
        "media/shaders/simple_flat_shader_program_fragment_shader.glsl"
    )
    {}
};

 // Lighting + Textures

class SimplePhongTextureShaderProgram : public LightingTextureShaderProgramBase
{
public:
    SimplePhongTextureShaderProgram() : LightingTextureShaderProgramBase(
        "media/shaders/simple_phong_texture_vertex_shader.glsl",
        "media/shaders/simple_phong_texture_fragment_shader.glsl"
    )
    {}
};

class SimpleGouraudTextureShaderProgram : public LightingTextureShaderProgramBase
{
public:
    SimpleGouraudTextureShaderProgram() : LightingTextureShaderProgramBase(
        "media/shaders/simple_gouraud_texture_vertex_shader.glsl",
        "media/shaders/simple_gouraud_texture_fragment_shader.glsl"
    )
    {}
};

class SimpleFlatTextureShaderProgram : public LightingTextureShaderProgramBase
{
public:
    SimpleFlatTextureShaderProgram() : LightingTextureShaderProgramBase(
        "media/shaders/simple_flat_texture_vertex_shader.glsl",
        "media/shaders/simple_flat_texture_fragment_shader.glsl"
    )
    {}
};

}