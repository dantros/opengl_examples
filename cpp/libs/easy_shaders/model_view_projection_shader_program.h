#pragma once

#include <string>
#include "load_shaders.h"
#include "gpu_shape.h"

namespace EasyShaders
{
struct SimpleModelViewProjectionShaderProgram
{
    GLuint shaderProgram;

    SimpleModelViewProjectionShaderProgram()
    {
        std::string const vertexShaderCode =
            "#version 130                                                         \n"
            "                                                                     \n"
            "uniform mat4 projection;                                             \n"
            "uniform mat4 view;                                                   \n"
            "uniform mat4 model;                                                  \n"
            "                                                                     \n"
            "in vec3 position;                                                    \n"
            "in vec3 color;                                                       \n"
            "                                                                     \n"
            "out vec3 newColor;                                                   \n"
            "void main()                                                          \n"
            "{                                                                    \n"
            "    gl_Position = projection * view * model * vec4(position, 1.0f);  \n"
            "    newColor = color;                                                \n"
            "}                                                                    \n";

        std::string const fragmentShaderCode =
            "#version 130                                                         \n"
            "in vec3 newColor;                                                    \n"
            "                                                                     \n"
            "out vec4 outColor;                                                   \n"
            "                                                                     \n"
            "void main()                                                          \n"
            "{                                                                    \n"
            "    outColor = vec4(newColor, 1.0f);                                 \n"
            "}                                                                    \n";

        shaderProgram = createShaderProgramFromCode({
            {GL_VERTEX_SHADER, vertexShaderCode.c_str()},
            {GL_FRAGMENT_SHADER, fragmentShaderCode.c_str()}
        });
    }

    void drawShape(GPUShape const& gpuShape, GLuint mode=GL_TRIANGLES) const
    {
        // Binding buffers
        glBindVertexArray(gpuShape.vao);
        glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo);

        // position attribute
        auto position = glGetAttribLocation(shaderProgram, "position");
        glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (void*)0);
        glEnableVertexAttribArray(position);

        // color attribute
        auto color = glGetAttribLocation(shaderProgram, "color");
        glVertexAttribPointer(color, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
        glEnableVertexAttribArray(color);

        // Render the active element buffer with the active shader program
        glDrawElements(mode, gpuShape.size, GL_UNSIGNED_INT, 0);

        // note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
        glBindBuffer(GL_ARRAY_BUFFER, 0); 

        // remember: do NOT unbind the EBO while a VAO is active as the bound element buffer object IS stored in the VAO; keep the EBO bound.
        //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
        // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
        glBindVertexArray(0); 
    }
};
}