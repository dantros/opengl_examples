#pragma once

#include <string>
#include <cassert>
#include <iostream>

#include <stb_image_interface.h>

#include <libs/shapes/shape.h>

namespace EasyShaders
{

struct GPUShape
{
    GLuint vao, vbo, ebo;
    std::size_t size;
    GLuint texture = 0;

    void clear()
    {
        glDeleteVertexArrays(1, &vao);
        glDeleteBuffers(1, &vbo);
        glDeleteBuffers(1, &ebo);
    }
};

inline GPUShape toGPUShape(Shapes::Shape const& shape)
{
    // Here the new shape will be stored
    GPUShape gpuShape;

    gpuShape.size = shape.indices.size();
    glGenVertexArrays(1, &(gpuShape.vao));
    glGenBuffers(1, &(gpuShape.vbo));
    glGenBuffers(1, &(gpuShape.ebo));

    // Vertex data must be attached to a Vertex Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo);
    glBufferData(GL_ARRAY_BUFFER, shape.vertices.size() * sizeof(GLfloat), shape.vertices.data(), GL_STATIC_DRAW);

    // Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, shape.indices.size() * sizeof(GLuint), shape.indices.data(), GL_STATIC_DRAW);

    return gpuShape;
}


inline GLuint textureSimpleSetup(std::string const& imgName, GLuint wrapMode, GLuint filterMode)
{
    // wrapMode: GL_REPEAT, GL_CLAMP_TO_EDGE
    // filterMode: GL_LINEAR, GL_NEAREST

    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    // texture wrapping params
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapMode);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapMode);

    // texture filtering params
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filterMode);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filterMode);

    // load image, create texture and generate mipmaps
	int width, height, nrChannels;
    unsigned char *data = stbi_load(imgName.c_str(), &width, &height, &nrChannels, 0);
    if (data == nullptr)
	{
        std::cout << "Failed to load texture" << std::endl;
        throw;
	}

    GLuint format;
    switch (nrChannels)
    {
    case 3:
        format = GL_RGB;
        break;
    case 4:
        format = GL_RGBA;
        break;
    default:
        std::cout << "nrChannels: " << nrChannels << std::endl;
        throw;
    };

    glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    stbi_image_free(data);

    return texture;
}

inline GPUShape toGPUShape(Shapes::Shape const& shape, GLuint wrapMode, GLuint filterMode)
{
    assert(shape.texture.size() != 0);

    GPUShape gpuShape = toGPUShape(shape);
    gpuShape.texture = textureSimpleSetup(shape.texture, wrapMode, filterMode);
    
    return gpuShape;
}

} //EasyShaders