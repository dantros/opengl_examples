
#include <Eigen/Core>
#include <Eigen/Dense>

//namespace SimpleEigen
//{

using Coord = float;

using Matrix4f = Eigen::Matrix<Coord,4,4,Eigen::DontAlign>;
using Matrix34f = Eigen::Matrix<Coord,3,4,Eigen::DontAlign>;
using Vector2f = Eigen::Matrix<Coord,2,1,Eigen::DontAlign>;
using Vector3f = Eigen::Matrix<Coord,3,1,Eigen::DontAlign>;
using Vector4f = Eigen::Matrix<Coord,4,1,Eigen::DontAlign>;

//} // namespace SimpleEigen