

namespace Grid
{

using GridIndex = std::size_t;

struct GridCoord
{
    GridIndex col, row;
};

struct GridSize
{
    std::size_t cols, rows;

    std::size_t length()
    {
        return cols * rows;
    }
};

GridIndex parametrize(GridSize const& gridSize, GridIndex col, GridIndex row)
{
    assert(col < gridSize.cols);
    assert(row < gridSize.rows);

    return gridSize.rows * row + col;
}

GridCoord deparametrize(GridSize const& gridSize, GridIndex index)
{
    assert(index < gridSize.length());

    GridIndex row = index / gridSize.rows;
    GridIndex col = index % gridSize.cols;
    
    return {col, row};
}

template <typename ObjectType>
class GridContainer
{
private:
    GridSize const gridSize;
    std::vector<ObjectType> data;

public:
    GridContainer(GridSize const& gridSize_) :
        gridSize(gridSize_),
        data{}
    {
        data.reserve(gridSize.length());
    }

    ObjectType const& operator[](GridIndex col, GridIndex row) const
    {
        GridIndex const index = parametrize(gridSize, col, row);
        return data.at(index);
    }

    ObjectType& operator[](GridIndex col, GridIndex row)
    {
        GridIndex const index = parametrize(gridSize, col, row);
        return data.at(index);
    }

    GridSize const& size() const
    {
        return gridSize;
    }

    std::vector<ObjectType> const& content() const
    {
        return data;
    }
};

} // namespace Grid