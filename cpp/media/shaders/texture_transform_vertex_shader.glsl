#version 130

uniform mat4 transform;

in vec3 position;
in vec2 texCoords;

out vec2 outTexCoords;

void main()
{
    gl_Position = transform * vec4(position, 1.0f);
    outTexCoords = texCoords;
}