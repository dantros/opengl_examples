#version 130

in vec3 position;
in vec2 texCoords;
in vec3 normal;

out vec2 fragTexCoords;
out vec3 vertexLightColor;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec3 lightPosition; 
uniform vec3 viewPosition; 
uniform vec3 La;
uniform vec3 Ld;
uniform vec3 Ls;
uniform vec3 Ka;
uniform vec3 Kd;
uniform vec3 Ks;
uniform uint shininess;
uniform float constantAttenuation;
uniform float linearAttenuation;
uniform float quadraticAttenuation;

void main()
{
    vec3 vertexPos = vec3(model * vec4(position, 1.0));
    gl_Position = projection * view * vec4(vertexPos, 1.0);

    fragTexCoords = texCoords;

    // ambient
    vec3 ambient = Ka * La;
    
    // diffuse 
    vec3 norm = normalize(normal);
    vec3 toLight = lightPosition - vertexPos;
    vec3 lightDir = normalize(toLight);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = Kd * Ld * diff;
    
    // specular
    vec3 viewDir = normalize(viewPosition - vertexPos);
    vec3 reflectDir = reflect(-lightDir, norm);  
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
    vec3 specular = Ks * Ls * spec;

    // attenuation
    float distToLight = length(toLight);
    float attenuation = constantAttenuation
        + linearAttenuation * distToLight
        + quadraticAttenuation * distToLight * distToLight;
    
    vertexLightColor = ambient + ((diffuse + specular) / attenuation);
}