
#version 130
            
uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

in vec3 position;
in vec2 texCoords;

out vec2 outTexCoords;

void main()
{
    gl_Position = projection * view * model * vec4(position, 1.0f);
    outTexCoords = texCoords;
}