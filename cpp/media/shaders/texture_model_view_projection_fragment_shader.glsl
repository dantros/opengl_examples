#version 130

uniform sampler2D samplerTex;

in vec2 outTexCoords;

out vec4 outColor;

void main()
{
    outColor = texture(samplerTex, outTexCoords);
}