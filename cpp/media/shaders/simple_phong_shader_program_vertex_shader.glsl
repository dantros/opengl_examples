#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;
layout (location = 2) in vec3 normal;

out vec3 fragPosition;
out vec3 fragOriginalColor;
out vec3 fragNormal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    fragPosition = vec3(model * vec4(position, 1.0));
    fragOriginalColor = color;
    fragNormal = mat3(transpose(inverse(model))) * normal;  
    
    gl_Position = projection * view * vec4(fragPosition, 1.0);
}