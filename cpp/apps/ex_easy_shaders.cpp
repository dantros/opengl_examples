#include <iostream>
#include <string>
#include <vector>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "libs/shapes/shape.h"
#include "libs/shapes/shapes_io.h"
#include "libs/easy_shaders/gpu_shape.h"
#include "libs/easy_shaders/model_view_projection_shader_program.h"
#include "libs/easy_shaders/shader_programs.h"
#include "libs/transformations/transformations.h"
#include "libs/basic_shapes/basic_shapes.h"

namespace sh = Shapes;
namespace bs = BasicShapes;
namespace es = EasyShaders;
namespace tr = Transformations;

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (action != GLFW_PRESS)
        return;

    if (key == GLFW_KEY_ESCAPE)
    {
        glfwSetWindowShouldClose(window, true);
    }
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

template <typename Pipeline>
void drawWithTransformShader(
	Pipeline const& pipeline,
	es::GPUShape const& gpuShape,
	tr::Matrix4f const& transform)
{
    // Note that viewPos vector and view matrix are correlated.
    glUseProgram(pipeline.shaderProgram);

    // Sending MVP matrices
	glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "transform"), 1, GL_FALSE, transform.data()); 

    // Drawing the shape
    pipeline.drawShape(gpuShape);
}

template <typename Pipeline>
void drawWithMvpShader(
	Pipeline const& pipeline,
	es::GPUShape const& gpuShape,
	tr::Matrix4f const& projection,
	tr::Matrix4f const& view,
	tr::Matrix4f const& model)
{
    // Note that viewPos vector and view matrix are correlated.
    glUseProgram(pipeline.shaderProgram);

    // Sending MVP matrices
	glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "projection"), 1, GL_FALSE, projection.data()); 
	glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "view"), 1, GL_FALSE, view.data());
    glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "model"), 1, GL_FALSE, model.data());

    // Drawing the shape
    pipeline.drawShape(gpuShape);
}

template <typename Pipeline>
void drawWithLigthingShader(
	Pipeline const& pipeline,
	es::GPUShape const& gpuShape,
	tr::Matrix4f const& projection,
	tr::Matrix4f const& view,
	tr::Matrix4f const& model,
    tr::Vector3f const viewPos)
{
    // Note that viewPos vector and view matrix are correlated.
    glUseProgram(pipeline.shaderProgram);

    // Sending MVP matrices
	glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "projection"), 1, GL_FALSE, projection.data()); 
	glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "view"), 1, GL_FALSE, view.data());
    glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "model"), 1, GL_FALSE, model.data());

    // Sending phong lighting parameters

    // White light in all components: ambient, diffuse and specular.
    glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "La"), 1.0, 1.0, 1.0);
    glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "Ld"), 1.0, 1.0, 1.0);
    glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "Ls"), 1.0, 1.0, 1.0);

    // Object is barely visible at only ambient. Diffuse behavior is slightly red. Sparkles are white
    glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "Ka"), 0.2, 0.2, 0.2);
    glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "Kd"), 0.9, 0.9, 0.9);
    glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "Ks"), 1.0, 1.0, 1.0);

    // TO DO: Explore different parameter combinations to understand their effect!

    glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "lightPosition"), -5,-5, 5);
    glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "viewPosition"), viewPos[0], viewPos[1], viewPos[2]);
    glUniform1ui(glGetUniformLocation(pipeline.shaderProgram, "shininess"), 100);
    
    glUniform1f(glGetUniformLocation(pipeline.shaderProgram, "constantAttenuation"), 0.0001);
    glUniform1f(glGetUniformLocation(pipeline.shaderProgram, "linearAttenuation"), 0.03);
    glUniform1f(glGetUniformLocation(pipeline.shaderProgram, "quadraticAttenuation"), 0.01);

    glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "projection"), 1, GL_FALSE, projection.data());
    glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "view"), 1, GL_FALSE, view.data());
    glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "model"), 1, GL_FALSE, model.data());

    // Drawing the shape
    pipeline.drawShape(gpuShape);
}

bs::Shape createDice(std::string const& image_filename)
{
    bs::Shape shape(5);
    shape.vertices = {
    //   positions         tex coords   normals
    // Z+: number 1
        -0.5f, -0.5f,  0.5f, 0.0f, 1.0f/3,
         0.5f, -0.5f,  0.5f, 0.5f, 1.0f/3,
         0.5f,  0.5f,  0.5f, 0.5f, 0.0f,  
        -0.5f,  0.5f,  0.5f, 0.0f, 0.0f,  

    // Z-: number 6
        -0.5f, -0.5f, -0.5f, 0.5f, 1.0f,  
         0.5f, -0.5f, -0.5f, 1.0f, 1.0f,  
         0.5f,  0.5f, -0.5f, 1.0f, 2.0f/3,
        -0.5f,  0.5f, -0.5f, 0.5f, 2.0f/3,
        
    // X+: number 5
         0.5f, -0.5f, -0.5f, 0.0f,   1.0f,
         0.5f,  0.5f, -0.5f, 0.5f,   1.0f,
         0.5f,  0.5f,  0.5f, 0.5f, 2.0f/3,
         0.5f, -0.5f,  0.5f, 0.0f, 2.0f/3,
 
    // X-: number 2
        -0.5f, -0.5f, -0.5f, 0.5f, 1.0f/3,
        -0.5f,  0.5f, -0.5f, 1.0f, 1.0f/3,
        -0.5f,  0.5f,  0.5f, 1.0f,   0.0f,
        -0.5f, -0.5f,  0.5f, 0.5f,   0.0f,

    // Y+: number 4
        -0.5f,  0.5f, -0.5f, 0.5f, 2.0f/3,
         0.5f,  0.5f, -0.5f, 1.0f, 2.0f/3,
         0.5f,  0.5f,  0.5f, 1.0f, 1.0f/3,
        -0.5f,  0.5f,  0.5f, 0.5f, 1.0f/3,

    // Y-: number 3
        -0.5f, -0.5f, -0.5f, 0.0f, 2.0f/3,
         0.5f, -0.5f, -0.5f, 0.5f, 2.0f/3,
         0.5f, -0.5f,  0.5f, 0.5f, 1.0f/3,
        -0.5f, -0.5f,  0.5f, 0.0f, 1.0f/3
    };
    // One face of the cube per row
    shape.indices = {
         0, 1, 2, 2, 3, 0, // Z+
         7, 6, 5, 5, 4, 7, // Z-
         8, 9,10,10,11, 8, // X+
        15,14,13,13,12,15, // X-
        19,18,17,17,16,19, // Y+
        20,21,22,22,23,20 // Y-
    };

    shape.texture = image_filename;

    return shape;
}

bs::Shape createLightingDice(std::string const& image_filename)
{
    bs::Shape shape(8);
    shape.vertices = {
    //   positions         tex coords   normals
    // Z+: number 1
        -0.5f, -0.5f,  0.5f, 0.0f, 1.0f/3,  0.0f, 0.0f, 1.0f,
         0.5f, -0.5f,  0.5f, 0.5f, 1.0f/3,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f, 0.5f, 0.0f,    0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f,  0.5f, 0.0f, 0.0f,    0.0f, 0.0f, 1.0f,

    // Z-: number 6
        -0.5f, -0.5f, -0.5f, 0.5f, 1.0f,      0.0f, 0.0f, -1.0f,
         0.5f, -0.5f, -0.5f, 1.0f, 1.0f,      0.0f, 0.0f, -1.0f,
         0.5f,  0.5f, -0.5f, 1.0f, 2.0f/3,    0.0f, 0.0f, -1.0f,
        -0.5f,  0.5f, -0.5f, 0.5f, 2.0f/3,    0.0f, 0.0f, -1.0f,
        
    // X+: number 5
         0.5f, -0.5f, -0.5f, 0.0f,   1.0f,   1.0f, 0.0f, 0.0f,
         0.5f,  0.5f, -0.5f, 0.5f,   1.0f,   1.0f, 0.0f, 0.0f,
         0.5f,  0.5f,  0.5f, 0.5f, 2.0f/3,   1.0f, 0.0f, 0.0f,
         0.5f, -0.5f,  0.5f, 0.0f, 2.0f/3,   1.0f, 0.0f, 0.0f,
 
    // X-: number 2
        -0.5f, -0.5f, -0.5f, 0.5f, 1.0f/3,   -1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f, 1.0f, 1.0f/3,   -1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f, 1.0f,   0.0f,   -1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f,  0.5f, 0.5f,   0.0f,   -1.0f, 0.0f, 0.0f,

    // Y+: number 4
        -0.5f,  0.5f, -0.5f, 0.5f, 2.0f/3,   0.0f, 1.0f, 0.0f,
         0.5f,  0.5f, -0.5f, 1.0f, 2.0f/3,   0.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f, 1.0f, 1.0f/3,   0.0f, 1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f, 0.5f, 1.0f/3,   0.0f, 1.0f, 0.0f,

    // Y-: number 3
        -0.5f, -0.5f, -0.5f, 0.0f, 2.0f/3,   0.0f, -1.0f, 0.0f,
         0.5f, -0.5f, -0.5f, 0.5f, 2.0f/3,   0.0f, -1.0f, 0.0f,
         0.5f, -0.5f,  0.5f, 0.5f, 1.0f/3,   0.0f, -1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f, 0.0f, 1.0f/3,   0.0f, -1.0f, 0.0f
    };
    // One face of the cube per row
    shape.indices = {
         0, 1, 2, 2, 3, 0, // Z+
         7, 6, 5, 5, 4, 7, // Z-
         8, 9,10,10,11, 8, // X+
        15,14,13,13,12,15, // X-
        19,18,17,17,16,19, // Y+
        20,21,22,22,23,20 // Y-
    };

    shape.texture = image_filename;

    return shape;
}

int main()
{
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_FALSE); // uncomment this statement to fix compilation on OS X
#endif

    // settings
    constexpr unsigned int SCR_WIDTH = 600;
    constexpr unsigned int SCR_HEIGHT = 600;

    // glfw window creation
    // --------------------
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "ex_easy_shaders", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    // Setting up callback functions
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetKeyCallback(window, key_callback);

    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    // Creating shaders and objects
    es::ColorTransformShaderProgram colorTransformPipeline;
    es::TextureTransformShaderProgram textureTransformPipeline;

    es::ColorModelViewProjectionShaderProgram colorMvpPipeline;
    es::TextureModelViewProjectionShaderProgram textureMvpPipeline;

    es::SimpleFlatColorShaderProgram flatColorPipeline;
    es::SimpleGouraudColorShaderProgram gouraudColorPipeline;
    es::SimplePhongColorShaderProgram phongColorPipeline;

    es::SimpleFlatTextureShaderProgram flatTexturePipeline;
    es::SimpleGouraudTextureShaderProgram gouraudTexturePipeline;
    es::SimplePhongTextureShaderProgram phongTexturePipeline;

    //Creating shapes on GPU memory
    es::GPUShape gpuAxis = es::toGPUShape(bs::createAxis(7));

    es::GPUShape gpuColorCircle = es::toGPUShape(bs::createColorCircle(20, 1, 0, 0));
    es::GPUShape gpuColorQuad = es::toGPUShape(bs::createRainbowQuad());
    es::GPUShape gpuTextureQuad = es::toGPUShape(bs::createTextureQuad("media/imgs/boo.png"), GL_REPEAT, GL_NEAREST);

    es::GPUShape gpuColorCube = es::toGPUShape(bs::createRainbowCube());
    es::GPUShape gpuTexture  = es::toGPUShape(createDice("media/imgs/dice_blue.jpg"), GL_REPEAT, GL_LINEAR);

    es::GPUShape gpuLightingCube = es::toGPUShape(bs::createRainbowNormalsCube());
    es::GPUShape gpuLightingTexture = es::toGPUShape(createLightingDice("media/imgs/dice_blue.jpg"), GL_REPEAT, GL_LINEAR);

    glClearColor(0.85f, 0.85f, 0.85f, 1.0f);

    // As we work in 3D, we need to check which part is in front,
    // and which one is at the back
    glEnable(GL_DEPTH_TEST);

    // Enabling transparencies
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    tr::Coord t0 = glfwGetTime(), t1, dt;
	tr::Coord cameraTheta = tr::PI / 4;

    tr::Matrix4f projection = tr::perspective(45, float(SCR_WIDTH)/float(SCR_HEIGHT), 0.1, 100);

    // render loop
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        // Using GLFW to check and process input events
        glfwPollEvents();            

        // Getting the time difference from the previous iteration
        t1 = glfwGetTime();
        dt = t1 - t0;
        t0 = t1;

        if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
            cameraTheta -= 2 * dt;

        if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
            cameraTheta += 2 * dt;

        tr::Vector3f const viewPos(
            10 * std::sin(cameraTheta),
            10 * std::cos(cameraTheta),
            5);
        tr::Vector3f const at(0,0,0);
        tr::Vector3f const up(0,0,1);

        tr::Matrix4f view = tr::lookAt(viewPos, at, up);

        //Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Drawing axis
        glUseProgram(colorMvpPipeline.shaderProgram);
        glUniformMatrix4fv(glGetUniformLocation(colorMvpPipeline.shaderProgram, "projection"), 1, GL_FALSE, projection.data()); 
        glUniformMatrix4fv(glGetUniformLocation(colorMvpPipeline.shaderProgram, "view"), 1, GL_FALSE, view.data());
        glUniformMatrix4fv(glGetUniformLocation(colorMvpPipeline.shaderProgram, "model"), 1, GL_FALSE, tr::identity().data());
		colorMvpPipeline.drawShape(gpuAxis, GL_LINES);

        // Drawing shapes
        drawWithTransformShader(colorTransformPipeline, gpuColorQuad, tr::translate(0.5,0.5,0) * tr::uniformScale(0.2));
        drawWithTransformShader(colorTransformPipeline, gpuColorCircle, tr::translate(0.0,0.5,0) * tr::uniformScale(0.2));
        drawWithTransformShader(textureTransformPipeline, gpuTextureQuad, tr::translate(-0.5,0.5,0) * tr::uniformScale(0.2));

        drawWithMvpShader(colorMvpPipeline, gpuColorCube, projection, view, tr::translate(-2,2,0));
        drawWithMvpShader(textureMvpPipeline, gpuTexture, projection, view, tr::translate(2,2,0));

        drawWithLigthingShader(flatColorPipeline, gpuLightingCube, projection, view, tr::translate(-2,0,0), viewPos);
        drawWithLigthingShader(gouraudColorPipeline, gpuLightingCube, projection, view, tr::translate(0,0,0), viewPos);
        drawWithLigthingShader(phongColorPipeline, gpuLightingCube, projection, view, tr::translate(2,0,0), viewPos);

        drawWithLigthingShader(flatTexturePipeline, gpuLightingTexture, projection, view, tr::translate(-2,-2,0), viewPos);
        drawWithLigthingShader(gouraudTexturePipeline, gpuLightingTexture, projection, view, tr::translate(0,-2,0), viewPos);
        drawWithLigthingShader(phongTexturePipeline, gpuLightingTexture, projection, view, tr::translate(2,-2,0), viewPos);

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    
    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}