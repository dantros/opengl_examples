#include <iostream>
#include <string>
#include <vector>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "libs/shapes/shape.h"
#include "libs/shapes/shapes_io.h"
#include "libs/easy_shaders/gpu_shape.h"
#include "libs/easy_shaders/shader_programs.h"
#include "libs/transformations/transformations.h"
#include "libs/basic_shapes/basic_shapes.h"
#include "libs/curves/simple_curves.h"

namespace sh = Shapes;
namespace bs = BasicShapes;
namespace es = EasyShaders;
namespace tr = Transformations;
namespace cs = Curves;

// A global variable to control the application
struct Controller
{
    bool fillPolygon = true;
    bool showAxis = true;
} controller;

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (action != GLFW_PRESS)
        return;

    if (key == GLFW_KEY_ESCAPE)
    {
        glfwSetWindowShouldClose(window, true);
    }
    else if (key == GLFW_KEY_SPACE)
    {
        controller.fillPolygon = not controller.fillPolygon;
    }
    else if (key == GLFW_KEY_LEFT_CONTROL)
    {
        controller.showAxis = not controller.showAxis;
    }
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

#if 0
inline Coord evalBernstein3(unsigned int k, Coord t)
{
    Matrix4f Mb;
    Mb << 1, -3, 3, -1,
        0, 3, -6, 3,
        0, 0, 3, -3,
        0, 0, 0, 1;

    auto const& rowK = Mb.row(k);
    auto const T = generateT(t);

    return rowK.dot(T);
}

using BezierPatchControlPoints = GridContainer<Vector3f>;

inline evalBezierPathSample(BezierPatchControlPoints const& controPoints, Coord t, Coord s)
{
    Vector3f Q = Vector3f::Zero();
    for (std::size_t k = 0; k < 4; k++)
    {
        for (std::size_t l = 0; l < 4; l++)
        {
            auto const Bk = evalBernstein3(k, s);
            auto const Bl = evalBernstein3(l, t);

            Q += Bk * Bl * controlPoints[k, l];
        }
    }
    return Q;
}
#endif
bs::Shape generateCurve(Matrix34f M, unsigned int N, tr::Vector3f color)
{
    bs::Shape shape(6);
    shape.vertices.reserve(6 * N);
    shape.indices.reserve(2 * N);

    for (unsigned int i = 0; i <= N; i++)
    {
		Coord const t = static_cast<Coord>(i) / N;
        Vector4f const T = cs::generateT(t);
		Vector3f const pointOnCurve = M * T;

        shape.vertices.push_back(pointOnCurve[0]);
        shape.vertices.push_back(pointOnCurve[1]);
        shape.vertices.push_back(pointOnCurve[2]);

        shape.vertices.push_back(color[0]);
        shape.vertices.push_back(color[1]);
        shape.vertices.push_back(color[2]);

        shape.indices.push_back(i);
        shape.indices.push_back(i + 1);
    }

    shape.indices.pop_back();

    return shape;
}

int main()
{
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_FALSE); // uncomment this statement to fix compilation on OS X
#endif

    // settings
    constexpr unsigned int SCR_WIDTH = 600;
    constexpr unsigned int SCR_HEIGHT = 600;

    // glfw window creation
    // --------------------
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "ex_surface", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    // Setting up callback functions
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetKeyCallback(window, key_callback);

    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    es::ColorModelViewProjectionShaderProgram pipeline;

    //Creating shapes on GPU memory
    es::GPUShape gpuAxis = es::toGPUShape(bs::createAxis(7));

    glClearColor(0.85f, 0.85f, 0.85f, 1.0f);

    // As we work in 3D, we need to check which part is in front,
    // and which one is at the back
    glEnable(GL_DEPTH_TEST);

    glUseProgram(pipeline.shaderProgram);

    tr::Coord t0 = glfwGetTime(), t1, dt;
	tr::Coord cameraTheta = tr::PI / 4;

    tr::Matrix4f projection = tr::perspective(45, float(SCR_WIDTH)/float(SCR_HEIGHT), 0.1, 100);

    /* Eigen Training */
#if 0
    BezierPatchControlPoints controlPoints({4,4});

    controlPoints[0, 0] = Vector3f(0, 0, 0);
    controlPoints[0, 1] = Vector3f(0, 1, 0);
    controlPoints[0, 2] = Vector3f(0, 2, 0);
    controlPoints[0, 3] = Vector3f(0, 3, 0);
    
    controlPoints[1, 0] = Vector3f(1, 0, 0);
    controlPoints[1, 1] = Vector3f(1, 1, 10);
    controlPoints[1, 2] = Vector3f(1, 2, 10);
    controlPoints[1, 3] = Vector3f(1, 3, 0);
    
    controlPoints[2, 0] = Vector3f(2, 0, 0);
    controlPoints[2, 1] = Vector3f(2, 1, 10);
    controlPoints[2, 2] = Vector3f(2, 2, 10);
    controlPoints[2, 3] = Vector3f(2, 3, 0);
    
    controlPoints[3, 0] = Vector3f(3, 0, 0);
    controlPoints[3, 1] = Vector3f(3, 1, -5);
    controlPoints[3, 2] = Vector3f(3, 2, -5);
    controlPoints[3, 3] = Vector3f(3, 3, 0);

#endif

    Vector3f const p1_(0, 0, 1),
        p2_(1, 0, 0),
        t1_(10, 0, 0),
        t2_(0, 10, 0);

    auto MH = cs::hermiteMatrix(p1_, p2_, t1_, t2_);
    bs::Shape hermiteShape = generateCurve(MH, 50, tr::Vector3f(1,0,0));
    es::GPUShape gpuHermite = es::toGPUShape(hermiteShape);

    Vector3f const r0_(0, 0, 1),
        r1_(0, 1, 0),
        r2_(1, 0, 1),
        r3_(1, 1, 0);

    auto MB = cs::bezierMatrix(r0_, r1_, r2_, r3_);
    bs::Shape bezierShape = generateCurve(MB, 50, tr::Vector3f(0,0,1));
    es::GPUShape gpuBezier = es::toGPUShape(bezierShape);

    bs::Shape grid = bs::createGridXY(10, 10, 0, 0.5, 0.5, 0.5);
    es::GPUShape gpuGrid = es::toGPUShape(grid);

    // render loop
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        // Using GLFW to check and process input events
        glfwPollEvents();

        // Filling or not the shapes depending on the controller state
        if (controller.fillPolygon)
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        else
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

        // Getting the time difference from the previous iteration
        t1 = glfwGetTime();
        dt = t1 - t0;
        t0 = t1;

        if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
            cameraTheta -= 2 * dt;

        if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
            cameraTheta += 2 * dt;

        tr::Vector3f const viewPos(
            8 * std::sin(cameraTheta),
            8 * std::cos(cameraTheta),
            4);
        tr::Vector3f const eye(0,0,0);
        tr::Vector3f const at(0,0,1);

        tr::Matrix4f view = tr::lookAt(viewPos, eye, at);

        //Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Drawing shapes with different model transformations
        glUseProgram(pipeline.shaderProgram);
        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "view"), 1, GL_FALSE, view.data());
        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "projection"), 1, GL_FALSE, projection.data());
        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "model"), 1, GL_FALSE, tr::identity().data());
		pipeline.drawShape(gpuAxis, GL_LINES);

        // Getting the shape to display
        pipeline.drawShape(gpuHermite, GL_LINES);
        pipeline.drawShape(gpuBezier, GL_LINES);

        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "model"), 1, GL_FALSE, tr::translate(-5, -5, 0).data());
        pipeline.drawShape(gpuGrid, GL_LINES);

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    
    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}