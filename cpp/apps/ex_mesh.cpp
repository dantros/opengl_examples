#include <iostream>
#include <string>
#include <vector>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "libs/shapes/shape.h"
#include "libs/shapes/shapes_io.h"
#include "libs/easy_shaders/gpu_shape.h"
#include "libs/easy_shaders/shader_programs.h"
#include "libs/transformations/transformations.h"
#include "libs/basic_shapes/basic_shapes.h"
#include "libs/curves/simple_curves.h"
#include "libs/mesh/mesh.h"
#include "libs/mesh/mesh_io.h"

namespace sh = Shapes;
namespace bs = BasicShapes;
namespace es = EasyShaders;
namespace tr = Transformations;
namespace cs = Curves;

// A global variable to control the application
struct Controller
{
    bool fillPolygon = true;
    bool showAxis = true;
} controller;

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (action != GLFW_PRESS)
        return;

    if (key == GLFW_KEY_ESCAPE)
    {
        glfwSetWindowShouldClose(window, true);
    }
    else if (key == GLFW_KEY_SPACE)
    {
        controller.fillPolygon = not controller.fillPolygon;
    }
    else if (key == GLFW_KEY_LEFT_CONTROL)
    {
        controller.showAxis = not controller.showAxis;
    }
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

using SimpleMesh = Mesh::PolygonMesh<Mesh::PositionVertex>;

SimpleMesh generatePiramid()
{
    SimpleMesh mesh;

	auto const top = mesh.insertVertex({{0,0,1}});
    auto const xpyp = mesh.insertVertex({{1,1,0}});
    auto const xpym = mesh.insertVertex({{1,-1,0}});
    auto const xmyp = mesh.insertVertex({{-1,1,0}});
    auto const xmym = mesh.insertVertex({{-1,-1,0}});

    mesh.insertConnection({xpym, xpyp, top});
    mesh.insertConnection({xpyp, xmyp, top});
    mesh.insertConnection({xmyp, xmym, top});
    mesh.insertConnection({xmym, xpym, top});
    mesh.insertConnection({xpyp, xmyp, xmym, xpym});

    return mesh;
}

bs::Shape toShape(SimpleMesh const& mesh, std::array<float, 3> color)
{
    bs::Shape shape(6);
    shape.vertices.reserve(6 * mesh.vertices.size());

    // The size of each connection is arbitrary if we use polygon connection.
    // We do not care if this is slow at this point in development.
    shape.vertices.reserve(4 * mesh.shapeConnections.size());

    for (auto const& vertex : mesh.vertices)
    {
        // position
        shape.vertices.push_back(vertex.position[0]);
        shape.vertices.push_back(vertex.position[1]);
        shape.vertices.push_back(vertex.position[2]);

        // color
        shape.vertices.push_back(color[0]);
        shape.vertices.push_back(color[1]);
        shape.vertices.push_back(color[2]);
    }

    for (auto const& connection : mesh.shapeConnections)
    {
        // TODO: add triangles every 3 indices in connection.
        // connection can have 3, 4 o any number of indices.
        for (auto const& index : connection)
        {
            shape.indices.push_back(index);
        }
    }

    return shape;
}

int main()
{
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_FALSE); // uncomment this statement to fix compilation on OS X
#endif

    // settings
    constexpr unsigned int SCR_WIDTH = 600;
    constexpr unsigned int SCR_HEIGHT = 600;

    // glfw window creation
    // --------------------
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "ex_curves", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    // Setting up callback functions
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetKeyCallback(window, key_callback);

    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    es::ColorModelViewProjectionShaderProgram pipeline;

    //Creating shapes on GPU memory
    es::GPUShape gpuAxis = es::toGPUShape(bs::createAxis(7));

    glClearColor(0.85f, 0.85f, 0.85f, 1.0f);

    // As we work in 3D, we need to check which part is in front,
    // and which one is at the back
    glEnable(GL_DEPTH_TEST);

    glUseProgram(pipeline.shaderProgram);

    tr::Coord t0 = glfwGetTime(), t1, dt;
	tr::Coord cameraTheta = tr::PI / 4;

    tr::Matrix4f projection = tr::perspective(45, float(SCR_WIDTH)/float(SCR_HEIGHT), 0.1, 100);

    bs::Shape grid = bs::createGridXY(10, 10, 0, 0.5, 0.5, 0.5);
    es::GPUShape gpuGrid = es::toGPUShape(grid);

    auto const piramidMesh = generatePiramid();
    std::cout << piramidMesh << std::endl;

    auto const piramidShape = toShape(piramidMesh, {0.5f, 0.5f, 1.0f});
    std::cout << piramidShape << std::endl;

    auto const piramidGpuShape = es::toGPUShape(piramidShape);

    // render loop
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        // Using GLFW to check and process input events
        glfwPollEvents();

        // Filling or not the shapes depending on the controller state
        if (controller.fillPolygon)
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        else
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

        // Getting the time difference from the previous iteration
        t1 = glfwGetTime();
        dt = t1 - t0;
        t0 = t1;

        if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
            cameraTheta -= 2 * dt;

        if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
            cameraTheta += 2 * dt;

        tr::Vector3f const viewPos(
            8 * std::sin(cameraTheta),
            8 * std::cos(cameraTheta),
            4);
        tr::Vector3f const eye(0,0,0);
        tr::Vector3f const at(0,0,1);

        tr::Matrix4f view = tr::lookAt(viewPos, eye, at);

        //Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Drawing shapes with different model transformations
        glUseProgram(pipeline.shaderProgram);
        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "view"), 1, GL_FALSE, view.data());
        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "projection"), 1, GL_FALSE, projection.data());
        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "model"), 1, GL_FALSE, tr::identity().data());
		pipeline.drawShape(gpuAxis, GL_LINES);

        // Getting the shape to display
        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "model"), 1, GL_FALSE, tr::translate(-5, -5, 0).data());
        pipeline.drawShape(gpuGrid, GL_LINES);

        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "model"), 1, GL_FALSE, tr::identity().data());
        
        pipeline.drawShape(piramidGpuShape);

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    
    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}