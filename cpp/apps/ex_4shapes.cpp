#include <iostream>
#include <string>
#include <vector>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <Eigen/Core>

#include "libs/shapes/shape.h"
#include "libs/shapes/shapes_io.h"
#include "libs/easy_shaders/load_shaders.h"
#include "libs/easy_shaders/gpu_shape.h"

#include "libs/transformations/transformations.h"

namespace sh = Shapes;
namespace es = EasyShaders;
namespace tr = Transformations;

// A global variable to control the application
struct Controller
{
    bool fillPolygon = true;
} controller;

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, true);
    }
    else if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
    {
        controller.fillPolygon = not controller.fillPolygon;
    }
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

es::GPUShape createRainbowTriangle()
{
    // Setting up a triangle
    sh::Shape shape(6);
    shape.vertices = {
    //   positions          colors
        -0.7f, -0.7f, 0.0f, 1.0f, 0.0f, 0.0f,
         0.7f, -0.7f, 0.0f, 0.0f, 1.0f, 0.0f,
         0.0f,  0.7f, 0.0f, 0.0f, 0.0f, 1.0f
    };
    shape.indices = {
        0, 1, 2
    };

    return es::toGPUShape(shape);
}

es::GPUShape createRainbowQuad()
{
    // Setting up 2 triangles to form a quad
    sh::Shape shape(6);
    shape.vertices = {
    //   positions           colors
        -0.5f, -0.5f, 0.0f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, 0.0f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f, 0.0f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f, 0.0f,  1.0f, 1.0f, 1.0f
    };
    shape.indices = {
        0, 1, 2,  // first triangle
        2, 3, 0   // second triangle
    };

    return es::toGPUShape(shape);
}

void drawShape(GLuint shaderProgram, es::GPUShape const& shape)
{
    // Binding buffers
    glBindVertexArray(shape.vao);
    glBindBuffer(GL_ARRAY_BUFFER, shape.vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, shape.ebo);

    // position attribute
    auto position = glGetAttribLocation(shaderProgram, "position");
    glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (void*)0);
    glEnableVertexAttribArray(position);

	// color attribute
    auto color = glGetAttribLocation(shaderProgram, "color");
	glVertexAttribPointer(color, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(color);

    // Render the active element buffer with the active shader program
    glDrawElements(GL_TRIANGLES, shape.size, GL_UNSIGNED_INT, 0);

    // note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0); 

    // remember: do NOT unbind the EBO while a VAO is active as the bound element buffer object IS stored in the VAO; keep the EBO bound.
    //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
    // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
    glBindVertexArray(0); 
}

int main()
{
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

    // settings
    constexpr unsigned int SCR_WIDTH = 600;
    constexpr unsigned int SCR_HEIGHT = 600;

    // glfw window creation
    // --------------------
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "ex_4shapes", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    // Setting up callback functions
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetKeyCallback(window, key_callback);

    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    std::string const vertexShaderCode =
        "#version 130                                        \n"
        "in vec3 position;                                   \n"
        "in vec3 color;                                      \n"
        "                                                    \n"
        "out vec3 fragColor;                                 \n"
        "                                                    \n"
        "uniform mat4 transform;                             \n"
        "                                                    \n"
        "void main()                                         \n"
        "{                                                   \n"
        "    fragColor = color;                              \n"
        "    gl_Position = transform * vec4(position, 1.0f); \n"
        "}                                                   \n";

    std::string const fragmentShaderCode =
        "#version 130                                        \n"
        "                                                    \n"
        "in vec3 fragColor;                                  \n"
        "out vec4 outColor;                                  \n"
        "                                                    \n"
        "void main()                                         \n"
        "{                                                   \n"
        "    outColor = vec4(fragColor, 1.0f);               \n"
        "}                                                   \n";

    auto shaderProgram = es::createShaderProgramFromCode({
        {GL_VERTEX_SHADER, vertexShaderCode.c_str()},
        {GL_FRAGMENT_SHADER, fragmentShaderCode.c_str()}
    });

    es::GPUShape gpuTriangle = createRainbowTriangle();
    es::GPUShape gpuQuad = createRainbowQuad();

    glClearColor(0.15f, 0.15f, 0.15f, 1.0f);
    glUseProgram(shaderProgram);

    // render loop
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        // Using GLFW to check and process input events
        glfwPollEvents();

        if (controller.fillPolygon)
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        else
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

        // render
        // ------
        glClear(GL_COLOR_BUFFER_BIT);

        tr::Coord theta = glfwGetTime();

        tr::Matrix4f triangleTransform = 
            tr::translate(0.5, 0.5, 0) * 
            tr::rotationZ(2 * theta) * 
            tr::uniformScale(0.5);

        // draw our first triangle
        glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "transform"), 1, GL_FALSE, triangleTransform.data());
        drawShape(shaderProgram, gpuTriangle);

        // Another instance of the triangle
        tr::Matrix4f triangleTransform2 = 
            tr::translate(-0.5, 0.5, 0) *
            tr::scale(
                0.5 + 0.2 * std::cos(1.5 * theta),
                0.5 + 0.2 * std::sin(2 * theta),
                0);

        glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "transform"), 1, GL_FALSE, triangleTransform2.data());
        drawShape(shaderProgram, gpuTriangle);

        //Quad
        tr::Matrix4f quadTransform = 
            tr::translate(-0.5, -0.5, 0) *
            tr::rotationZ(-theta) *
            tr::uniformScale(0.7);

        glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "transform"), 1, GL_FALSE, quadTransform.data());
        drawShape(shaderProgram, gpuQuad);

        // Another instance of the Quad
        tr::Matrix4f quadTransform2 =
            tr::translate(0.5, -0.5, 0) *
            tr::shearing(0.3 * std::cos(theta), 0, 0, 0, 0, 0) *
            tr::uniformScale(0.7);

        glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "transform"), 1, GL_FALSE, quadTransform2.data());
        drawShape(shaderProgram, gpuQuad);

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    gpuTriangle.clear();
    gpuQuad.clear();

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}