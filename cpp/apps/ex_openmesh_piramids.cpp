
#define _USE_MATH_DEFINES

#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
#include "libs/shapes/shape.h"
#include "libs/shapes/shapes_io.h"
#include "libs/easy_shaders/gpu_shape.h"
#include "libs/easy_shaders/shader_programs.h"
#include "libs/transformations/transformations.h"
#include "libs/basic_shapes/basic_shapes.h"

namespace sh = Shapes;
namespace bs = BasicShapes;
namespace es = EasyShaders;
namespace tr = Transformations;

using PolyMesh = OpenMesh::PolyMesh_ArrayKernelT<>;

struct Controller
{
    bool fillPolygon = true;
    bool showAxis = true;
} controller;

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (action != GLFW_PRESS)
        return;

    if (key == GLFW_KEY_ESCAPE)
    {
        glfwSetWindowShouldClose(window, true);
    }
    else if (key == GLFW_KEY_SPACE)
    {
        controller.fillPolygon = not controller.fillPolygon;
    }
    else if (key == GLFW_KEY_LEFT_CONTROL)
    {
        controller.showAxis = not controller.showAxis;
    }
}

struct MeshBuilder
{
    PolyMesh::VertexHandle insertVertex(PolyMesh::Point const& point)
    {
        return mesh.add_vertex(point);
    }

    void insertFace(std::vector<PolyMesh::VertexHandle> vertexHandles)
    {
        std::vector<PolyMesh::VertexHandle> face_vhandles;
        face_vhandles.reserve(vertexHandles.size());
        
        for (auto& vertexHandle : vertexHandles)
        
            face_vhandles.push_back(vertexHandle);

        mesh.add_face(face_vhandles);
    }

    PolyMesh mesh;
};

PolyMesh generatePiramid()
{
    MeshBuilder meshBuilder;

	auto const top = meshBuilder.insertVertex({0,0,1});
    auto const xpyp = meshBuilder.insertVertex({1,1,0});
    auto const xpym = meshBuilder.insertVertex({1,-1,0});
    auto const xmyp = meshBuilder.insertVertex({-1,1,0});
    auto const xmym = meshBuilder.insertVertex({-1,-1,0});

    meshBuilder.insertFace({xmym, xmyp, xpyp, xpym});
    meshBuilder.insertFace({xpym, xpyp, top});
    meshBuilder.insertFace({xpyp, xmyp, top});
    meshBuilder.insertFace({xmyp, xmym, top});
    meshBuilder.insertFace({xmym, xpym, top});

    return meshBuilder.mesh;
}

PolyMesh generateCube()
{
    MeshBuilder meshBuilder;

    auto const xpypzm = meshBuilder.insertVertex({1,1,-1});
    auto const xpymzm = meshBuilder.insertVertex({1,-1,-1});
    auto const xmypzm = meshBuilder.insertVertex({-1,1,-1});
    auto const xmymzm = meshBuilder.insertVertex({-1,-1,-1});

    auto const xpypzp = meshBuilder.insertVertex({1,1,1});
    auto const xpymzp = meshBuilder.insertVertex({1,-1,1});
    auto const xmypzp = meshBuilder.insertVertex({-1,1,1});
    auto const xmymzp = meshBuilder.insertVertex({-1,-1,1});

    // z- face
    meshBuilder.insertFace({xmymzm, xmypzm, xpypzm, xpymzm});
    // z+ face
    meshBuilder.insertFace({xpymzp, xpypzp, xmypzp, xmymzp});
    // x- face
    meshBuilder.insertFace({xmymzp, xmypzp, xmypzm, xmymzm});
    // x+ face
    meshBuilder.insertFace({xpymzm, xpypzm, xpypzp, xpymzp});
    // y- face
    meshBuilder.insertFace({xmymzm, xpymzm, xpymzp, xmymzp});
    // y+ face
    meshBuilder.insertFace({xmypzm, xmypzp, xpypzp, xpypzm});
    
    return meshBuilder.mesh;
}

sh::Indices extractFaceIndices(PolyMesh const& mesh, OpenMesh::PolyConnectivity::ConstFaceIter faceIt)
{
    std::size_t numberOfIndices = mesh.valence( *faceIt );

    sh::Indices faceIndices;
    faceIndices.reserve(numberOfIndices);

    for (auto faceVertexIt = mesh.cfv_iter(*faceIt); faceVertexIt.is_valid(); ++faceVertexIt)
    {
        auto const index = faceVertexIt->idx();
        faceIndices.push_back(index);
    }

    return faceIndices;
}

void addTriangleFace(sh::Indices& indices, sh::Indices const& faceIndices)
{
    assert(faceIndices.size() == 3);

    indices.insert(indices.end(), faceIndices.begin(), faceIndices.end());
}

void addConvexPolygonalFace(sh::Indices& indices, sh::Indices const& faceIndices, sh::Index offset=0)
{
    assert(faceIndices.size() >= 3);

    sh::Index first = faceIndices.at(0) + offset;

    for (sh::Index index = 1; index < faceIndices.size()-1; ++index)
    {
        auto const& second = faceIndices.at(index) + offset;
        auto const& third = faceIndices.at(index + 1) + offset;

        addTriangleFace(indices, {first, second, third});
    }
}

bs::Shape toColorShape(PolyMesh const& mesh, std::array<float, 3> color)
{
    assert(not mesh.has_vertex_normals());

    bs::Shape shape(6);

    for (auto vertexIt = mesh.vertices_begin(); vertexIt != mesh.vertices_end(); ++vertexIt)
    {
        auto const& vertex = mesh.point(*vertexIt);
        
        // position
        shape.vertices.push_back(vertex[0]);
        shape.vertices.push_back(vertex[1]);
        shape.vertices.push_back(vertex[2]);

        // color
        shape.vertices.push_back(color[0]);
        shape.vertices.push_back(color[1]);
        shape.vertices.push_back(color[2]);
    }

    for (auto faceIt = mesh.faces_begin(); faceIt != mesh.faces_end(); ++faceIt)
    {
        auto const faceIndices = extractFaceIndices(mesh, faceIt);
        addConvexPolygonalFace(shape.indices, faceIndices);
    }

    return shape;
}

bs::Shape toShapeNormalsPerVertex(PolyMesh const& mesh, std::array<float, 3> color)
{
    assert(mesh.has_vertex_normals());

    bs::Shape shape(9);

    for (auto vertexIt = mesh.vertices_begin(); vertexIt != mesh.vertices_end(); ++vertexIt)
    {
        auto const& vertex = mesh.point(*vertexIt);
        
        // position
        shape.vertices.push_back(vertex[0]);
        shape.vertices.push_back(vertex[1]);
        shape.vertices.push_back(vertex[2]);

        // color
        shape.vertices.push_back(color[0]);
        shape.vertices.push_back(color[1]);
        shape.vertices.push_back(color[2]);
        
        // normal
        auto const& normal = mesh.normal(*vertexIt);
        shape.vertices.push_back(normal[0]);
        shape.vertices.push_back(normal[1]);
        shape.vertices.push_back(normal[2]);

    }

    for (auto faceIt = mesh.faces_begin(); faceIt != mesh.faces_end(); ++faceIt)
    {
        auto const faceIndices = extractFaceIndices(mesh, faceIt);
        addConvexPolygonalFace(shape.indices, faceIndices);
    }

    return shape;
}

void addFace(bs::Shape& shape, PolyMesh const& mesh, OpenMesh::PolyConnectivity::ConstFaceIter faceIt, std::array<float, 3> color)
{
    std::size_t numberOfVertices = mesh.valence( *faceIt );

    std::cout << "addFace " << numberOfVertices << std::endl;
    if (numberOfVertices != 3 and numberOfVertices != 4)
    {
        std::cout << "Face with " << numberOfVertices << " are not supported. It has been skipped." << std::endl;
        return;
    }

    auto const face_normal = mesh.normal(*faceIt);

    std::size_t offset = shape.vertices.size() / shape.stride;

    for (auto vertexIt = mesh.cfv_iter(*faceIt); vertexIt; ++vertexIt)
    {
        // position
        auto const position = mesh.point(*vertexIt);
        shape.vertices.push_back(position[0]);
        shape.vertices.push_back(position[1]);
        shape.vertices.push_back(position[2]);

        // color
        shape.vertices.push_back(color[0]);
        shape.vertices.push_back(color[1]);
        shape.vertices.push_back(color[2]);

        // normal
        shape.vertices.push_back(face_normal[0]);
        shape.vertices.push_back(face_normal[1]);
        shape.vertices.push_back(face_normal[2]);
    }

    if (numberOfVertices == 3)
        addConvexPolygonalFace(shape.indices, {0,1,2}, offset);
    else
        addConvexPolygonalFace(shape.indices, {0,1,2,3}, offset);
}

bs::Shape toShapeNormalsPerFace(PolyMesh const& mesh, std::array<float, 3> color)
{
    assert(mesh.has_face_normals());

    bs::Shape shape(9);

    for (auto faceIt = mesh.faces_begin(); faceIt != mesh.faces_end(); ++faceIt)
    {
        addFace(shape, mesh, faceIt, color);
    }

    return shape;
}

int main()
{
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // settings
    constexpr unsigned int SCR_WIDTH = 600;
    constexpr unsigned int SCR_HEIGHT = 600;

    // glfw window creation
    // --------------------
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "ex_openmesh_piramids", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    // Setting up callback functions
    glfwSetKeyCallback(window, key_callback);

    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    es::ColorModelViewProjectionShaderProgram colorPipeline;
    es::SimplePhongColorShaderProgram phongPipeline;

    //Creating shapes on GPU memory
    es::GPUShape gpuAxis = es::toGPUShape(bs::createAxis(7));

    glClearColor(0.85f, 0.85f, 0.85f, 1.0f);

    // As we work in 3D, we need to check which part is in front,
    // and which one is at the back
    glEnable(GL_DEPTH_TEST);

    tr::Coord t0 = glfwGetTime(), t1, dt;
	tr::Coord cameraTheta = tr::PI / 4;

    tr::Matrix4f projection = tr::perspective(45, float(SCR_WIDTH)/float(SCR_HEIGHT), 0.1, 100);

    bs::Shape grid = bs::createGridXY(10, 10, 0, 0.5, 0.5, 0.5);
    es::GPUShape gpuGrid = es::toGPUShape(grid);

    //auto piramidMesh = generatePiramid();
    auto piramidMesh = generateCube();

    auto const piramidShape = toColorShape(piramidMesh, {0.5f, 0.5f, 1.0f});
    //std::cout << piramidShape << std::endl;

    auto const piramidGpuShape = es::toGPUShape(piramidShape);

    piramidMesh.request_face_normals();
    piramidMesh.request_vertex_normals();
    piramidMesh.update_normals();
    
    auto const normalsPiramidShape = toShapeNormalsPerVertex(piramidMesh, {1.0f, 0.0f, 0.0f});
    //auto const normalsPiramidShape = toShapeNormalsPerFace(piramidMesh, {1.0f, 0.0f, 0.0f});

    std::cout << "normalsPiramidShape " << normalsPiramidShape << std::endl;

    std::cout << "vertices " << normalsPiramidShape.vertices.size() / normalsPiramidShape.stride << std::endl;
    std::cout << "indices  " << normalsPiramidShape.indices.size() << std::endl;

    auto const normalsPiramidGpuShape = es::toGPUShape(normalsPiramidShape);

    // render loop
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        // Using GLFW to check and process input events
        glfwPollEvents();

        // Filling or not the shapes depending on the controller state
        if (controller.fillPolygon)
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        else
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

        // Getting the time difference from the previous iteration
        t1 = glfwGetTime();
        dt = t1 - t0;
        t0 = t1;

        if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
            cameraTheta -= 2 * dt;

        if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
            cameraTheta += 2 * dt;

        tr::Vector3f const viewPos(
            8 * std::sin(cameraTheta),
            8 * std::cos(cameraTheta),
            4);
        tr::Vector3f const eye(0,0,0);
        tr::Vector3f const at(0,0,1);

        tr::Matrix4f view = tr::lookAt(viewPos, eye, at);

        //Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Drawing shapes with different model transformations
        glUseProgram(colorPipeline.shaderProgram);
        glUniformMatrix4fv(glGetUniformLocation(colorPipeline.shaderProgram, "view"), 1, GL_FALSE, view.data());
        glUniformMatrix4fv(glGetUniformLocation(colorPipeline.shaderProgram, "projection"), 1, GL_FALSE, projection.data());

        glUniformMatrix4fv(glGetUniformLocation(colorPipeline.shaderProgram, "model"), 1, GL_FALSE, tr::identity().data());
		colorPipeline.drawShape(gpuAxis, GL_LINES);
        glUniformMatrix4fv(glGetUniformLocation(colorPipeline.shaderProgram, "model"), 1, GL_FALSE, tr::translate(-5, -5, 0).data());
        colorPipeline.drawShape(gpuGrid, GL_LINES);
        glUniformMatrix4fv(glGetUniformLocation(colorPipeline.shaderProgram, "model"), 1, GL_FALSE, tr::translate(-3, -3, 0).data());
        colorPipeline.drawShape(piramidGpuShape);

        glUseProgram(phongPipeline.shaderProgram);

        // White light in all components: ambient, diffuse and specular.
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "La"), 1.0, 1.0, 1.0);
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Ld"), 1.0, 1.0, 1.0);
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Ls"), 1.0, 1.0, 1.0);

        // Object is barely visible at only ambient. Diffuse behavior is slightly red. Sparkles are white
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Ka"), 0.2, 0.2, 0.2);
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Kd"), 0.9, 0.5, 0.5);
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Ks"), 1.0, 1.0, 1.0);

        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "lightPosition"), -5, -5, 5);
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "viewPosition"), viewPos[0], viewPos[1], viewPos[2]);
        glUniform1ui(glGetUniformLocation(phongPipeline.shaderProgram, "shininess"), 100);

        glUniform1f(glGetUniformLocation(phongPipeline.shaderProgram, "constantAttenuation"), 0.0001);
        glUniform1f(glGetUniformLocation(phongPipeline.shaderProgram, "linearAttenuation"), 0.03);
        glUniform1f(glGetUniformLocation(phongPipeline.shaderProgram, "quadraticAttenuation"), 0.01);

        glUniformMatrix4fv(glGetUniformLocation(phongPipeline.shaderProgram, "view"), 1, GL_FALSE, view.data());
        glUniformMatrix4fv(glGetUniformLocation(phongPipeline.shaderProgram, "projection"), 1, GL_FALSE, projection.data());

        glUniformMatrix4fv(glGetUniformLocation(phongPipeline.shaderProgram, "model"), 1, GL_FALSE, tr::uniformScale(0.5).data());
        phongPipeline.drawShape(normalsPiramidGpuShape);

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    
    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();

    return 0;
}