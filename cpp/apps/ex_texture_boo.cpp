#include <iostream>
#include <string>
#include <vector>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "libs/shapes/shape.h"
#include "libs/shapes/shapes_io.h"
#include "libs/easy_shaders/gpu_shape.h"
#include "libs/easy_shaders/texture_transform_shader_program.h"
#include "libs/transformations/transformations.h"
#include "libs/basic_shapes/basic_shapes.h"

namespace sh = Shapes;
namespace bs = BasicShapes;
namespace es = EasyShaders;
namespace tr = Transformations;

// A global variable to control the application
struct Controller
{
    bool fillPolygon = true;
} controller;

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, true);
    }
    else if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
    {
        controller.fillPolygon = not controller.fillPolygon;
    }
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

int main()
{
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_FALSE); // uncomment this statement to fix compilation on OS X
#endif

    // settings
    constexpr unsigned int SCR_WIDTH = 600;
    constexpr unsigned int SCR_HEIGHT = 600;

    // glfw window creation
    // --------------------
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "ex_texture_boo", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    // Setting up callback functions
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetKeyCallback(window, key_callback);

    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    es::SimpleTextureTransformShaderProgram pipeline;

    //Creating shapes on GPU memory
    es::GPUShape gpuBoo           = es::toGPUShape(bs::createTextureQuad("media/imgs/boo.png"), GL_REPEAT, GL_NEAREST);
    es::GPUShape gpuQuestionBoxes = es::toGPUShape(bs::createTextureQuad("media/imgs/question_box.png", 10, 1), GL_REPEAT, GL_NEAREST);

    // Enabling transparencies
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glClearColor(0.15f, 0.15f, 0.15f, 1.0f);

    // As we work in 3D, we need to check which part is in front,
    // and which one is at the back
    //glEnable(GL_DEPTH_TEST);

    glUseProgram(pipeline.shaderProgram);

    tr::Coord theta, tx, ty, dtx;

    tr::Matrix4f questionBoxesTransform = tr::translate(0, -0.8, 0) * tr::scale(2, 0.2, 1);

    // render loop
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        // Using GLFW to check and process input events
        glfwPollEvents();

        // Filling or not the shapes depending on the controller state
        if (controller.fillPolygon)
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        else
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

        // Getting the time difference from the previous iteration
        theta = glfwGetTime();
        tx = 0.7 * std::sin(0.5 * theta);
        ty = 0.2 * std::sin(5 * theta);
    
        // derivative of tx give us the direction
        // dtx = 0.7 * 0.5 * std::cos(0.5 * theta); // only 'cos' contributes to the sign
        dtx = std::cos(0.5 * theta);
        
        tr::Matrix4f reflex;
        if (dtx > 0)
            reflex = tr::identity();
        else
            reflex = tr::scale(-1, 1, 1);

        glClear(GL_COLOR_BUFFER_BIT);

        tr::Matrix4f booTransform = tr::translate(tx, ty, 0) * tr::scale(0.5, 0.5, 1.0) * reflex;
        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "transform"), 1, GL_FALSE, booTransform.data());
        pipeline.drawShape(gpuBoo);

        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "transform"), 1, GL_FALSE, questionBoxesTransform.data());
        pipeline.drawShape(gpuQuestionBoxes);

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    gpuBoo.clear();
    gpuQuestionBoxes.clear();

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}