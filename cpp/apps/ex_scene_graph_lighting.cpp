#include <iostream>
#include <string>
#include <vector>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "libs/shapes/shape.h"
#include "libs/shapes/shapes_io.h"
#include "libs/easy_shaders/gpu_shape.h"
#include "libs/easy_shaders/model_view_projection_shader_program.h"
#include "libs/easy_shaders/shader_programs.h"
#include "libs/transformations/transformations.h"
#include "libs/transformations/scene_graph.h"
#include "libs/basic_shapes/basic_shapes.h"

namespace sh = Shapes;
namespace bs = BasicShapes;
namespace es = EasyShaders;
namespace tr = Transformations;

// A global variable to control the application
struct Controller
{
    bool fillPolygon = true;
    bool showAxis = true;
} controller;

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (action != GLFW_PRESS)
        return;

    if (key == GLFW_KEY_ESCAPE)
    {
        glfwSetWindowShouldClose(window, true);
    }
    else if (key == GLFW_KEY_SPACE)
    {
        controller.fillPolygon = not controller.fillPolygon;
    }
    else if (key == GLFW_KEY_LEFT_CONTROL)
    {
        controller.showAxis = not controller.showAxis;
    }
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

tr::SceneGraphNodePtr createCar(tr::Coord r, tr::Coord g, tr::Coord b)
{
    // only gpu shapes created
    auto gpuChasisPtr = std::make_shared<es::GPUShape>(es::toGPUShape(bs::createColorNormalsCube(r,g,b)));
    auto gpuWeelPtr = std::make_shared<es::GPUShape>(es::toGPUShape(bs::createColorNormalsCube(0,0,0)));

    // The wheel object
    auto wheelPtr = std::make_shared<tr::SceneGraphNode>("wheel", tr::scale(0.2, 0.8, 0.2), gpuWeelPtr);

    // A node to control wheel rotations
    auto wheelRotationPtr = std::make_shared<tr::SceneGraphNode>("wheelRotation");
    wheelRotationPtr->childs.push_back(wheelPtr);

    // creating wheels
    auto frontWheelPtr = std::make_shared<tr::SceneGraphNode>("frontWheel", tr::translate(0.3,0,-0.3));
    frontWheelPtr->childs.push_back(wheelRotationPtr);

    auto backWheelPtr = std::make_shared<tr::SceneGraphNode>("backWheel", tr::translate(-0.3,0,-0.3));
    backWheelPtr->childs.push_back(wheelRotationPtr);

    // Creating the chasis of the car
    auto chasisPtr = std::make_shared<tr::SceneGraphNode>("chasis", tr::scale(1,0.7,0.5), gpuChasisPtr);

    auto carPtr = std::make_shared<tr::SceneGraphNode>("car");
    carPtr->childs.push_back(chasisPtr);
    carPtr->childs.push_back(frontWheelPtr);
    carPtr->childs.push_back(backWheelPtr);

    return carPtr;
}

int main()
{
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_FALSE); // uncomment this statement to fix compilation on OS X
#endif

    // settings
    constexpr unsigned int SCR_WIDTH = 600;
    constexpr unsigned int SCR_HEIGHT = 600;

    // glfw window creation
    // --------------------
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "ex_scene_graph", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    // Setting up callback functions
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetKeyCallback(window, key_callback);

    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    es::SimpleModelViewProjectionShaderProgram colorPipeline;
    es::SimplePhongColorShaderProgram phongPipeline;

    //Creating shapes on GPU memory
    es::GPUShape gpuAxis = es::toGPUShape(bs::createAxis(7));
    tr::SceneGraphNodePtr sgRedCarPtr = createCar(1,0,0);
    tr::SceneGraphNodePtr sgBlueCarPtr = createCar(0,0,1);

    sgBlueCarPtr->transform = tr::rotationZ(-tr::PI/4) * tr::translate(3.0,0,0.5);

    glClearColor(0.85f, 0.85f, 0.85f, 1.0f);

    // As we work in 3D, we need to check which part is in front,
    // and which one is at the back
    glEnable(GL_DEPTH_TEST);

    unsigned int numberOfFrames = 0;
    tr::Coord t0 = glfwGetTime(), t1, dt, tSeg = 0.0f;
	tr::Coord cameraTheta = tr::PI / 4;

    tr::Matrix4f projection = tr::perspective(45, float(SCR_WIDTH)/float(SCR_HEIGHT), 0.1, 100);

    // render loop
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        // Using GLFW to check and process input events
        glfwPollEvents();

        // Filling or not the shapes depending on the controller state
        if (controller.fillPolygon)
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        else
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

        // Getting the time difference from the previous iteration
        t1 = glfwGetTime();
        dt = t1 - t0;
        t0 = t1;

        // Setting window title
        tSeg += dt;
        numberOfFrames++;
        if (tSeg > 0.5f)
        {
            int fps = static_cast<float>(numberOfFrames) / dt;
            tSeg = 0.0f;
            numberOfFrames = 0;;

            std::stringstream ss;
            ss << "ex_scene_graph [" << fps << " fps]";

            glfwSetWindowTitle(window, ss.str().c_str());
        }

        if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
            cameraTheta -= 2 * dt;

        if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
            cameraTheta += 2 * dt;

        tr::Vector3f const viewPos(
            8 * std::sin(cameraTheta),
            8 * std::cos(cameraTheta),
            4);
        tr::Vector3f const eye(0,0,0);
        tr::Vector3f const at(0,0,1);

        tr::Matrix4f view = tr::lookAt(viewPos, eye, at);


        sgRedCarPtr->transform = tr::translate(3 * std::sin( t1 ),0,0.5);
        auto redWheelRotationNodeMaybe = tr::findNode(sgRedCarPtr, "wheelRotation");
        
        assert(redWheelRotationNodeMaybe.has_value());
        
        tr::SceneGraphNode& redWheelRotationNode = *(redWheelRotationNodeMaybe.value());

        redWheelRotationNode.transform = tr::rotationY(-10 * t1);

        // Uncomment to print the red car position on every iteration
        /*auto positionMaybe = tr::findPosition(sgRedCarPtr, "car");
        assert(positionMaybe.has_value());
        auto& position = positionMaybe.value();
        std::cout << position << std::endl;*/

        //Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Drawing shapes with different model transformations
        glUseProgram(colorPipeline.shaderProgram);
        glUniformMatrix4fv(glGetUniformLocation(colorPipeline.shaderProgram, "view"), 1, GL_FALSE, view.data());
        glUniformMatrix4fv(glGetUniformLocation(colorPipeline.shaderProgram, "projection"), 1, GL_FALSE, projection.data());
        glUniformMatrix4fv(glGetUniformLocation(colorPipeline.shaderProgram, "model"), 1, GL_FALSE, tr::identity().data());
		colorPipeline.drawShape(gpuAxis, GL_LINES);
        
        glUseProgram(phongPipeline.shaderProgram);

        // Sending MVP matrices
        glUniformMatrix4fv(glGetUniformLocation(phongPipeline.shaderProgram, "projection"), 1, GL_FALSE, projection.data()); 
        glUniformMatrix4fv(glGetUniformLocation(phongPipeline.shaderProgram, "view"), 1, GL_FALSE, view.data());

        // Sending phong lighting parameters

        // White light in all components: ambient, diffuse and specular.
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "La"), 1.0, 1.0, 1.0);
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Ld"), 1.0, 1.0, 1.0);
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Ls"), 1.0, 1.0, 1.0);

        // Object is barely visible at only ambient. Diffuse behavior is slightly red. Sparkles are white
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Ka"), 0.2, 0.2, 0.2);
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Kd"), 0.9, 0.9, 0.9);
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "Ks"), 1.0, 1.0, 1.0);

        // TO DO: Explore different parameter combinations to understand their effect!

        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "lightPosition"), -5,-5, 5);
        glUniform3f(glGetUniformLocation(phongPipeline.shaderProgram, "viewPosition"), viewPos[0], viewPos[1], viewPos[2]);
        glUniform1ui(glGetUniformLocation(phongPipeline.shaderProgram, "shininess"), 100);
        
        glUniform1f(glGetUniformLocation(phongPipeline.shaderProgram, "constantAttenuation"), 0.0001);
        glUniform1f(glGetUniformLocation(phongPipeline.shaderProgram, "linearAttenuation"), 0.03);
        glUniform1f(glGetUniformLocation(phongPipeline.shaderProgram, "quadraticAttenuation"), 0.01);

        // Getting the shape to display
        drawSceneGraphNode(sgRedCarPtr, phongPipeline, "model");
        drawSceneGraphNode(sgBlueCarPtr, phongPipeline, "model");

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    
    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}