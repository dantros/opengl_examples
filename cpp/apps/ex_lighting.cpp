#include <iostream>
#include <string>
#include <vector>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "libs/shapes/shape.h"
#include "libs/shapes/shapes_io.h"
#include "libs/easy_shaders/gpu_shape.h"
#include "libs/easy_shaders/model_view_projection_shader_program.h"
#include "libs/easy_shaders/shader_programs.h"
#include "libs/transformations/transformations.h"
#include "libs/basic_shapes/basic_shapes.h"

namespace sh = Shapes;
namespace bs = BasicShapes;
namespace es = EasyShaders;
namespace tr = Transformations;

enum class ShadingType{Flat, Gouraud, Phong};

// A global variable to control the application
struct Controller
{
    bool fillPolygon = true;
    bool showAxis = true;
    ShadingType shadingType = ShadingType::Phong;
    int shapeIndex = 0;
} controller;

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (action != GLFW_PRESS)
        return;

    if (key == GLFW_KEY_ESCAPE)
    {
        glfwSetWindowShouldClose(window, true);
    }
    else if (key == GLFW_KEY_SPACE)
    {
        controller.fillPolygon = not controller.fillPolygon;
    }

    else if (key == GLFW_KEY_1)
    {
        controller.shadingType = ShadingType::Flat;
    }

    else if (key == GLFW_KEY_2)
    {
        controller.shadingType = ShadingType::Gouraud;
    }

    else if (key == GLFW_KEY_3)
    {
        controller.shadingType = ShadingType::Phong;
    }

    else if (key == GLFW_KEY_LEFT_CONTROL)
    {
        controller.showAxis = not controller.showAxis;
    }
    else if (key == GLFW_KEY_Q)
    {
        controller.shapeIndex--;
    }
    else if (key == GLFW_KEY_W)
    {
        controller.shapeIndex++;
    }
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

template <typename Pipeline>
void drawWithLigthingShader(
	Pipeline const& pipeline,
	es::GPUShape const& gpuShape,
	tr::Matrix4f const& projection,
	tr::Matrix4f const& view,
	tr::Matrix4f const& model,
    tr::Vector3f const viewPos)
{
    // Note that viewPos vector and view matrix are correlated.

    glUseProgram(pipeline.shaderProgram);

    // Sending MVP matrices
	glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "projection"), 1, GL_FALSE, projection.data()); 
	glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "view"), 1, GL_FALSE, view.data());
    glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "model"), 1, GL_FALSE, model.data());

    // Sending phong lighting parameters

    // White light in all components: ambient, diffuse and specular.
    glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "La"), 1.0, 1.0, 1.0);
    glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "Ld"), 1.0, 1.0, 1.0);
    glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "Ls"), 1.0, 1.0, 1.0);

    // Object is barely visible at only ambient. Diffuse behavior is slightly red. Sparkles are white
    glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "Ka"), 0.2, 0.2, 0.2);
    glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "Kd"), 0.9, 0.5, 0.5);
    glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "Ks"), 1.0, 1.0, 1.0);

    // TO DO: Explore different parameter combinations to understand their effect!

    glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "lightPosition"), -5, -5, 5);
    glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "viewPosition"), viewPos[0], viewPos[1], viewPos[2]);
    glUniform1ui(glGetUniformLocation(pipeline.shaderProgram, "shininess"), 100);
    
    glUniform1f(glGetUniformLocation(pipeline.shaderProgram, "constantAttenuation"), 0.0001);
    glUniform1f(glGetUniformLocation(pipeline.shaderProgram, "linearAttenuation"), 0.03);
    glUniform1f(glGetUniformLocation(pipeline.shaderProgram, "quadraticAttenuation"), 0.01);

    glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "projection"), 1, GL_FALSE, projection.data());
    glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "view"), 1, GL_FALSE, view.data());
    glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "model"), 1, GL_FALSE, model.data());

    // Drawing the shape
    pipeline.drawShape(gpuShape);
}

int main()
{
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_FALSE); // uncomment this statement to fix compilation on OS X
#endif

    // settings
    constexpr unsigned int SCR_WIDTH = 600;
    constexpr unsigned int SCR_HEIGHT = 600;

    // glfw window creation
    // --------------------
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "ex_lighting", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    // Setting up callback functions
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetKeyCallback(window, key_callback);

    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    es::SimpleModelViewProjectionShaderProgram pipeline;
    es::SimpleFlatColorShaderProgram flatPipeline;
    es::SimpleGouraudColorShaderProgram gouraudPipeline;
    es::SimplePhongColorShaderProgram phongPipeline;

    //Creating shapes on GPU memory
    es::GPUShape gpuAxis = es::toGPUShape(bs::createAxis(7));

    std::vector<es::GPUShape> gpuShapes
    {
        es::toGPUShape(bs::createRainbowNormalsCube()),
        es::toGPUShape(bs::createColorNormalsCube(1,0,0)),
        es::toGPUShape(bs::createColorNormalsCube(0,1,0)),
        es::toGPUShape(bs::createColorNormalsCube(0,0,1)),
        es::toGPUShape(bs::createColorNormalsCube(1,1,0)),
        es::toGPUShape(bs::createColorNormalsCube(0,1,1)),
        es::toGPUShape(bs::createColorNormalsCube(1,0,1)),
        es::toGPUShape(bs::createColorNormalsCube(1,1,1)),
        es::toGPUShape(bs::createColorNormalsCube(0.5,0.5,0.5)),
        es::toGPUShape(bs::createColorNormalsCube(0,0,0)),
    };

    glClearColor(0.15f, 0.15f, 0.15f, 1.0f);

    // As we work in 3D, we need to check which part is in front,
    // and which one is at the back
    glEnable(GL_DEPTH_TEST);

    glUseProgram(pipeline.shaderProgram);

    tr::Coord t0 = glfwGetTime(), t1, dt;
	tr::Coord cameraTheta = tr::PI / 4;

    tr::Matrix4f projection = tr::perspective(45, float(SCR_WIDTH)/float(SCR_HEIGHT), 0.1, 100);
    tr::Matrix4f model = tr::identity();

    // render loop
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        // Using GLFW to check and process input events
        glfwPollEvents();

        // Filling or not the shapes depending on the controller state
        if (controller.fillPolygon)
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        else
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

        // Getting the time difference from the previous iteration
        t1 = glfwGetTime();
        dt = t1 - t0;
        t0 = t1;

        if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
            cameraTheta -= 2 * dt;

        if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
            cameraTheta += 2 * dt;

        tr::Vector3f const viewPos(
            3 * std::sin(cameraTheta),
            3 * std::cos(cameraTheta),
            2);
        tr::Vector3f const eye(0,0,0);
        tr::Vector3f const at(0,0,1);

        tr::Matrix4f view = tr::lookAt(viewPos, eye, at);

        //Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Drawing shapes with different model transformations
        glUseProgram(pipeline.shaderProgram);
        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "view"), 1, GL_FALSE, view.data());
        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "projection"), 1, GL_FALSE, projection.data());
        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "model"), 1, GL_FALSE, tr::identity().data());
		pipeline.drawShape(gpuAxis, GL_LINES);

        // Cyclic rotation over shapes.
        if (controller.shapeIndex == gpuShapes.size())
            controller.shapeIndex = 0;
        else if (controller.shapeIndex < 0)
            controller.shapeIndex = gpuShapes.size() - 1;

        // Getting the shape to display
        es::GPUShape const& shapeToDisplay = gpuShapes.at(controller.shapeIndex);

        switch (controller.shadingType)
        {
            case ShadingType::Flat:
                drawWithLigthingShader(flatPipeline, shapeToDisplay, projection, view, model, viewPos);
                break;
            case ShadingType::Gouraud:
                drawWithLigthingShader(gouraudPipeline, shapeToDisplay, projection, view, model, viewPos);
                break;
            case ShadingType::Phong:
                drawWithLigthingShader(phongPipeline, shapeToDisplay, projection, view, model, viewPos);
                break;
            default:
                throw;
        }

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    gpuAxis.clear();
    for (auto& gpuShape : gpuShapes)
        gpuShape.clear();
    
    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}